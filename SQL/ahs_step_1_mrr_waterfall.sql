--step 1

INSERT INTO etl_acct_health.mrr_1_rpc_apd
(select * from src_zuora.rateplancharge_history rate_pc
where (rate_pc.amendmenttype in ('NewProduct') or nullif(rate_pc.amendmenttype,'') is null)
and rate_pc.sbs_status = 'Active'
and rpc_name not like ('%API $100 - overage fee%')
and rpc_name not like ('%1 Month%') 
and rpc_name not like ('%Adjustment%')
and dt = current_date - 1);



---step 2

INSERT INTO etl_acct_health.mrr_2_rpc_waterfall_apd
With lst as
(select dt as lst_load_date,
acc_number as lst_acc_number,
acc_id as lst_acc_id,
pd_name as lst_pd_name,
rpc_originalid as lst_rpc_originalid,
acc_name as lst_acc_name,
rpc_name as lst_rpc_name,
rpc_quantity as lst_rpc_quantity,
rpc_billingperiod as lst_billingperiod,
rpc_currentterm as lst_currentterm,
rpc_mrr as lst_mrr,
row_number() over(partition by acc_number,rpc_name,rpc_originalid order by rpc_mrr ) as rn
from etl_acct_health.mrr_1_rpc_apd
where  dt = (current_date - 2) ),
curr as
(select acc_id,
acc_number,
pd_name,
acc_name,
sbs_status,
sbs_sales_channel__c,
sbs_cancellation_reason__c,
sbs_subscriptionstartdate as subscriptionstartdate,
sbs_serviceactivationdate,
sbs_cancelleddate,
sbs_originalid,
sbs_free_months_included__c,
rpc_originalid,
rpc_effectivestartdate,
rpc_effectiveenddate,
rpc_name,
amendmenttype,
rpc_mrr,
rpc_quantity,
rpc_billingperiod as billingperiod,
row_number() over(partition by acc_number,rpc_name,rpc_originalid order by rpc_mrr) rn,
rpc_currentterm as currentterm,
dt as load_date
from etl_acct_health.mrr_1_rpc_apd where dt = current_date -1 ),
T2 as
(SELECT coalesce(curr.acc_id,lst.lst_acc_id) as acc_id,
coalesce(curr.acc_number,lst.lst_acc_number) as acc_number,
coalesce(curr.pd_name,lst.lst_pd_name) as pd_name,
coalesce(curr.acc_name,lst.lst_acc_name) as acc_name,
curr.sbs_status,
curr.sbs_sales_channel__c,
curr.sbs_cancellation_reason__c,
curr.subscriptionstartdate,
curr.sbs_serviceactivationdate,
curr.sbs_cancelleddate,
curr.sbs_originalid,
curr.sbs_free_months_included__c,
curr.rpc_effectivestartdate,
curr.rpc_effectiveenddate,
coalesce(curr.rpc_name,lst.lst_rpc_name) as rpc_name,
curr.amendmenttype,
curr.rpc_originalid,
curr.rpc_mrr,
curr.rpc_quantity,
coalesce(lst.lst_rpc_quantity,0) as rpc_quantity_entry,
(coalesce(curr.rpc_quantity,0) - coalesce(lst.lst_rpc_quantity,0)) as rpc_quantity_change,
coalesce(lst.lst_mrr, 0)AS mrr_entry,
(coalesce(curr.rpc_mrr, 0) - coalesce(lst.lst_mrr, 0)) AS MRR_Change ,
 CASE WHEN ((coalesce(lst.lst_mrr,0) > 0 ) AND (coalesce(lst.lst_mrr, 0) > curr.rpc_mrr) AND (curr.rpc_mrr > 0)) THEN 'Downsell'
      WHEN ((coalesce(lst.lst_mrr,0) > 0 ) AND (coalesce(lst.lst_mrr, 0) < curr.rpc_mrr)) THEN 'Upsell'
      WHEN ((coalesce(lst.lst_mrr, 0) <=0 ) AND curr.rpc_mrr > 0) THEN 'New'
      WHEN ((coalesce(lst.lst_mrr, 0) > 0 ) AND (coalesce(curr.rpc_mrr,0) <= 0)) THEN 'Cancel'
      WHEN ((coalesce(lst.lst_mrr, 0) = curr.rpc_mrr)) THEN 'Static'
      ELSE 'Undefined'
 END AS MRR_Change_Type  ,
 coalesce(curr.billingperiod,lst.lst_billingperiod) as billingperiod,
 coalesce(curr.currentterm,lst.lst_currentterm) as currentterm,
 coalesce(curr.load_date,(dateadd('day', 1, lst.lst_load_date))) as load_date,
 lst.lst_load_date
FROM curr full outer join lst
on lst.lst_acc_number = curr.acc_number and
lst.lst_rpc_originalid = curr.rpc_originalid 
and UPPER(lst.lst_rpc_name) = UPPER(curr.rpc_name) 
and lst.rn = curr.rn 
and curr.load_date = dateadd('day', 1, lst.lst_load_date)
)
SELECT T2.acc_id,
 T2.acc_number,
 T2.pd_name,
 T2.acc_name,
 T2.sbs_status,
 T2.sbs_sales_channel__c,
 T2.sbs_cancellation_reason__c,
 T2.subscriptionstartdate,
 T2.sbs_serviceactivationdate,
 T2.sbs_cancelleddate,
 T2.sbs_originalid,
 T2.sbs_free_months_included__c,
 T2.rpc_effectivestartdate,
 T2.rpc_effectiveenddate,
 T2.rpc_originalid,
 T2.rpc_name,
 T2.rpc_quantity,
 T2.rpc_quantity_entry,
 T2.rpc_quantity_change,
 T2.amendmenttype,
 T2.billingperiod,
 T2.currentterm,
 T2.rpc_mrr,
 T2.mrr_entry,
 T2.MRR_Change,
 T2.MRR_Change_Type,


CASE WHEN T2.MRR_Change_Type = 'New' THEN T2.MRR_Change
      ELSE 0
      END AS mrr_new,
CASE  WHEN T2.MRR_Change_Type = 'Upsell' THEN T2.MRR_Change
      ELSE 0
      END AS  mrr_upsell,
CASE  WHEN T2.MRR_Change_Type = 'Downsell' THEN T2.MRR_Change
      ELSE 0
      END AS  mrr_downsell,
CASE  WHEN T2.MRR_Change_Type = 'Cancel' THEN T2.MRR_Change
      ELSE 0
      END AS  mrr_cancel,
CASE  WHEN T2.MRR_Change_Type = 'Static' THEN T2.MRR_Change
      ELSE 0
      END AS MRR_Static,
(T2.mrr_entry + T2.mrr_change) as mrr_exit,
T2.load_date,
T2.lst_load_date
FROM T2 order by T2.acc_number,T2.rpc_name;
-----
-----step 3


INSERT INTO etl_acct_health.mrr_3_agg_ad  (
select acc_number,load_date,
       sum(rpc_mrr) as rpc_mrr,
       sum(mrr_entry) as mrr_entry,
       sum(mrr_change) as mrr_change,
       sum(mrr_new) as mrr_new,
       sum(mrr_upsell) as mrr_upsell,
       sum(mrr_downsell) as mrr_downsell,
       sum(mrr_cancel)as mrr_cancel,
       sum(mrr_static) as mrr_static,
       sum(mrr_exit) as mrr_exit from etl_acct_health.mrr_2_rpc_waterfall_apd 
group by acc_number,load_date )

----Step 4----

INSERT INTO etl_acct_health.mrr_4_agg_waterfall_ad 
With lst as
(select load_date as lst_load_date,
        acc_number as lst_acc_number,
        mrr_exit as lst_mrr
from etl_acct_health.mrr_3_agg_ad
where load_date = (current_date -2) ),
curr as
(select acc_number,
        mrr_exit,
        load_date
from etl_acct_health.mrr_3_agg_ad where  load_date = current_date - 1),
T2 as
(SELECT curr.acc_number,
        coalesce(lst.lst_mrr, 0)AS mrr_entry,
        (coalesce(curr.mrr_exit, 0) - coalesce(lst.lst_mrr, 0)) AS MRR_Change ,
        CASE WHEN ((coalesce(lst.lst_mrr,0) > 0 ) AND (coalesce(lst.lst_mrr, 0) > curr.mrr_exit) AND (curr.mrr_exit > 0)) THEN 'Downsell'
             WHEN ((coalesce(lst.lst_mrr,0) > 0 ) AND (coalesce(lst.lst_mrr, 0) < curr.mrr_exit)) THEN 'Upsell'
             WHEN ((coalesce(lst.lst_mrr, 0) <=0 ) AND curr.mrr_exit > 0) THEN 'New'
             WHEN ((coalesce(lst.lst_mrr, 0) > 0 ) AND (coalesce(curr.mrr_exit,0) <= 0)) THEN 'Cancel'
             WHEN ((coalesce(lst.lst_mrr, 0) = curr.mrr_exit)) THEN 'Static'
        ELSE 'Undefined'
        END AS MRR_Change_Type  ,
        curr.load_date
FROM curr left outer join lst
on lst.lst_acc_number = curr.acc_number  and curr.load_date = dateadd('day', 1, lst.lst_load_date)
)
SELECT T2.acc_number,
       T2.mrr_entry,
       T2.MRR_Change,
       T2.MRR_Change_Type,
CASE WHEN T2.MRR_Change_Type = 'New' THEN T2.MRR_Change
     ELSE 0
     END AS mrr_new,
CASE WHEN T2.MRR_Change_Type = 'Upsell' THEN T2.MRR_Change
     ELSE 0
     END AS  mrr_upsell,
CASE WHEN T2.MRR_Change_Type = 'Downsell' THEN T2.MRR_Change
     ELSE 0
     END AS  mrr_downsell,
CASE WHEN T2.MRR_Change_Type = 'Cancel' THEN T2.MRR_Change
     ELSE 0
     END AS  mrr_cancel,
CASE WHEN T2.MRR_Change_Type = 'Static' THEN T2.MRR_Change
     ELSE 0
END AS MRR_Static,
(T2.mrr_entry + T2.mrr_change) as mrr_exit,
T2.load_date
FROM T2 order by T2.acc_number,T2.load_date;





