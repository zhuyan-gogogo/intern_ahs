
# for sql branch
unload ('select * from etl_acct_health.ahs_3_ml_ad  where sales_group = \'SB\'
and ahs_date >= \'2017-03-01\' and ahs_date < \'2017-04-01\'')
to 's3://zoom-datascience/temp/yan/'
with credentials 'aws_access_key_id=AKIAIIHCL6YTJDCQQA4Q;aws_secret_access_key=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'
DELIMITER AS '\t' ALLOWOVERWRITE;


#get the last file <data file>
aws s3 ls s3://zoom-datascience/temp/yan/

#copy to the current directory
aws s3 cp s3://zoom-datascience/temp/yan/<data file> intern_ahs/data/train_data.csv

#check whether copied successfully, look for the size
ls -ltr


# do the same for test data
unload ('select * from etl_acct_health.ahs_3_ml_ad  where sales_group = \'SB\'
and ahs_date >= \'2017-04-01\' and ahs_date < \'2017-05-01\'')
to 's3://zoom-datascience/temp/yan/'
with credentials 'aws_access_key_id=AKIAIIHCL6YTJDCQQA4Q;aws_secret_access_key=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'
DELIMITER AS '\t' ALLOWOVERWRITE;


#get the last file <data file>
aws s3 ls s3://zoom-datascience/temp/yan/

#copy to the current directory
aws s3 cp s3://zoom-datascience/temp/yan/<data file> intern_ahs/data/test_data.csv

#check whether copied successfully, look for the size
ls -ltr


aws s3 cp s3://zoom-datascience/acct_health_score/tabledata/mm_may.csv intern_ahs/data/train_data.csv
aws s3 cp s3://zoom-datascience/acct_health_score/tabledata/mm_may_add.csv intern_ahs/data/test_data.csv

aws s3 cp s3://zoom-datascience/temp/yan/ml_pipeline.py intern_ahs/models/


aws s3 cp intern_ahs/models/cv_parameter_mod.py s3://zoom-datascience/temp/yan/
aws s3 cp intern_ahs/models/preprocess.py s3://zoom-datascience/temp/yan/



names = ['ahs_date','zoom_account_no','account_name','account_created_date','account_age','sales_group','employee_count','coreproduct','currentterm',
    'data_com_industry','billtostate','billtocountry','mrr_entry','mrr_exit','mrr_growth_30','mrr_growth_60','mrr_growth_90','mrr_growth_180','billing_hosts',
    'paid_user_count','billing_hosts_utilization','paid_user_mau','paid_user_utilization','free_user_count','free_user_mau','free_user_utilization',
    'total_zr_deployed','total_meetings_last_30','total_minutes_last_30','meetings_per_user_last_30','minutes_per_user_last_30','total_minutes_per_meeting_last_30',
    'paid_meetings_last_30','paid_minutes_last_30','paid_meetings_per_user_last_30','paid_minutes_per_user_last_30','paid_minutes_per_meeting_last_30',
    'free_meetings_last_30','free_minutes_last_30','free_meetings_per_user_last_30','free_minutes_per_user_last_30','free_minutes_per_meeting_last_30',
    'zr_meetings_last_30','zr_minutes_last_30','zr_meetings_per_zr_last_30','zr_minutes_per_zr_last_30','zr_minutes_per_meeting_last_30','daily_zr_meeting_attach_rate',
    'daily_zr_minutes_attach_rate','webinar_meetings_last_30','webinar_minutes_last_30','webinar_minutes_per_meeting_last_30','unique_host_last_30days',
    'unique_logins_last_30days','billing_hosts_growth_30','billing_hosts_growth_60','billing_hosts_growth_90','billing_hosts_growth_180','billing_hosts_util_growth_30',
    'billing_hosts_util_growth_60','billing_hosts_util_growth_90','billing_hosts_util_growth_180','free_mthly_min_growth_30','free_mthly_min_growth_60',
    'free_mthly_min_growth_90','free_mthly_min_growth_180','free_mthly_min_per_mtg_growth_30','free_mthly_min_per_mtg_growth_60','free_mthly_min_per_mtg_growth_90',
    'free_mthly_min_per_mtg_growth_180','free_mthly_min_per_user_growth_30','free_mthly_min_per_user_growth_60','free_mthly_min_per_user_growth_90',
    'free_mthly_min_per_user_growth_180','free_mthly_mtgs_growth_30','free_mthly_mtgs_growth_60','free_mthly_mtgs_growth_90','free_mthly_mtgs_growth_180',
    'free_mthly_mtgs_per_user_growth_30','free_mthly_mtgs_per_user_growth_60','free_mthly_mtgs_per_user_growth_90','free_mthly_mtgs_per_user_growth_180',
    'free_user_count_growth_30','free_user_count_growth_60','free_user_count_growth_90','free_user_count_growth_180','free_user_mau_growth_30','free_user_mau_growth_60',
    'free_user_mau_growth_90','free_user_mau_growth_180','free_user_util_growth_30','free_user_util_growth_60','free_user_util_growth_90','free_user_util_growth_180',
    'mthly_min_per_mtg_growth_30','mthly_min_per_mtg_growth_60','mthly_min_per_mtg_growth_90','mthly_min_per_mtg_growth_180','mthly_min_per_user_growth_30',
    'mthly_min_per_user_growth_60','mthly_min_per_user_growth_90','mthly_min_per_user_growth_180','mthly_mtgs_per_user_growth_30','mthly_mtgs_per_user_growth_60',
    'mthly_mtgs_per_user_growth_90','mthly_mtgs_per_user_growth_180','paid_mthly_min_growth_30','paid_mthly_min_growth_60','paid_mthly_min_growth_90',
    'paid_mthly_min_growth_180','paid_mthly_min_per_mtg_growth_30','paid_mthly_min_per_mtg_growth_60','paid_mthly_min_per_mtg_growth_90','paid_mthly_min_per_mtg_growth_180',
    'paid_mthly_min_per_user_growth_30','paid_mthly_min_per_user_growth_60','paid_mthly_min_per_user_growth_90','paid_mthly_min_per_user_growth_180',
    'paid_mthly_mtgs_growth_30','paid_mthly_mtgs_growth_60','paid_mthly_mtgs_growth_90','paid_mthly_mtgs_growth_180','paid_mthly_mtgs_per_user_growth_30',
    'paid_mthly_mtgs_per_user_growth_60','paid_mthly_mtgs_per_user_growth_90','paid_mthly_mtgs_per_user_growth_180','paid_user_count_growth_30','paid_user_count_growth_60',
    'paid_user_count_growth_90','paid_user_count_growth_180','paid_user_mau_growth_30','paid_user_mau_growth_60','paid_user_mau_growth_90','paid_user_mau_growth_180',
    'paid_user_util_growth_30','paid_user_util_growth_60','paid_user_util_growth_90','paid_user_util_growth_180','total_mthly_min_growth_30','total_mthly_min_growth_60',
    'total_mthly_min_growth_90','total_mthly_min_growth_180','total_mthly_mtgs_growth_30','total_mthly_mtgs_growth_60','total_mthly_mtgs_growth_90','total_mthly_mtgs_growth_180',
    'opp_amount','days_left_in_term','churn_downsell','churn_cancel','churn_gross','churn_downsell_next_90','churn_cancel_next_90','churn_gross_next_90']
