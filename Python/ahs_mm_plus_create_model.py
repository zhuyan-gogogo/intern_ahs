#################################################
############# building the model ################
#################################################

###64GB ec2 instance

ssh ec2-user@52.91.193.93


###32GB ec2 instance

ssh ec2-user@54.174.221.32

###run python in datascience instance

sudo su - datascience

python

###Import libraries

import io
import pandas as pd
import numpy as np
from sklearn.ensemble import GradientBoostingClassifier 
from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation, metrics 
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score
import boto3

###import data file

s3 = boto3.client('s3')
obj = s3.get_object(Bucket='zoom-datascience',Key = 'acct_health_score/' + 'tabledata/' + 'mm.csv')
file = obj["Body"].read()
df = pd.read_csv(io.BytesIO(file),names = ['ahs_date','zoom_account_no','account_name','account_created_date','account_age','sales_group','employee_count','coreproduct','currentterm',
    'data_com_industry','billtostate','billtocountry','mrr_entry','mrr_exit','mrr_growth_30','mrr_growth_60','mrr_growth_90','mrr_growth_180','billing_hosts',
    'paid_user_count','billing_hosts_utilization','paid_user_mau','paid_user_utilization','free_user_count','free_user_mau','free_user_utilization',
    'total_zr_deployed','total_meetings_last_30','total_minutes_last_30','meetings_per_user_last_30','minutes_per_user_last_30','total_minutes_per_meeting_last_30',
    'paid_meetings_last_30','paid_minutes_last_30','paid_meetings_per_user_last_30','paid_minutes_per_user_last_30','paid_minutes_per_meeting_last_30',
    'free_meetings_last_30','free_minutes_last_30','free_meetings_per_user_last_30','free_minutes_per_user_last_30','free_minutes_per_meeting_last_30',
    'zr_meetings_last_30','zr_minutes_last_30','zr_meetings_per_zr_last_30','zr_minutes_per_zr_last_30','zr_minutes_per_meeting_last_30','daily_zr_meeting_attach_rate',
    'daily_zr_minutes_attach_rate','webinar_meetings_last_30','webinar_minutes_last_30','webinar_minutes_per_meeting_last_30','unique_host_last_30days',
    'unique_logins_last_30days','billing_hosts_growth_30','billing_hosts_growth_60','billing_hosts_growth_90','billing_hosts_growth_180','billing_hosts_util_growth_30',
    'billing_hosts_util_growth_60','billing_hosts_util_growth_90','billing_hosts_util_growth_180','free_mthly_min_growth_30','free_mthly_min_growth_60',
    'free_mthly_min_growth_90','free_mthly_min_growth_180','free_mthly_min_per_mtg_growth_30','free_mthly_min_per_mtg_growth_60','free_mthly_min_per_mtg_growth_90',
    'free_mthly_min_per_mtg_growth_180','free_mthly_min_per_user_growth_30','free_mthly_min_per_user_growth_60','free_mthly_min_per_user_growth_90',
    'free_mthly_min_per_user_growth_180','free_mthly_mtgs_growth_30','free_mthly_mtgs_growth_60','free_mthly_mtgs_growth_90','free_mthly_mtgs_growth_180',
    'free_mthly_mtgs_per_user_growth_30','free_mthly_mtgs_per_user_growth_60','free_mthly_mtgs_per_user_growth_90','free_mthly_mtgs_per_user_growth_180',
    'free_user_count_growth_30','free_user_count_growth_60','free_user_count_growth_90','free_user_count_growth_180','free_user_mau_growth_30','free_user_mau_growth_60',
    'free_user_mau_growth_90','free_user_mau_growth_180','free_user_util_growth_30','free_user_util_growth_60','free_user_util_growth_90','free_user_util_growth_180',
    'mthly_min_per_mtg_growth_30','mthly_min_per_mtg_growth_60','mthly_min_per_mtg_growth_90','mthly_min_per_mtg_growth_180','mthly_min_per_user_growth_30',
    'mthly_min_per_user_growth_60','mthly_min_per_user_growth_90','mthly_min_per_user_growth_180','mthly_mtgs_per_user_growth_30','mthly_mtgs_per_user_growth_60',
    'mthly_mtgs_per_user_growth_90','mthly_mtgs_per_user_growth_180','paid_mthly_min_growth_30','paid_mthly_min_growth_60','paid_mthly_min_growth_90',
    'paid_mthly_min_growth_180','paid_mthly_min_per_mtg_growth_30','paid_mthly_min_per_mtg_growth_60','paid_mthly_min_per_mtg_growth_90','paid_mthly_min_per_mtg_growth_180',
    'paid_mthly_min_per_user_growth_30','paid_mthly_min_per_user_growth_60','paid_mthly_min_per_user_growth_90','paid_mthly_min_per_user_growth_180',
    'paid_mthly_mtgs_growth_30','paid_mthly_mtgs_growth_60','paid_mthly_mtgs_growth_90','paid_mthly_mtgs_growth_180','paid_mthly_mtgs_per_user_growth_30',
    'paid_mthly_mtgs_per_user_growth_60','paid_mthly_mtgs_per_user_growth_90','paid_mthly_mtgs_per_user_growth_180','paid_user_count_growth_30','paid_user_count_growth_60',
    'paid_user_count_growth_90','paid_user_count_growth_180','paid_user_mau_growth_30','paid_user_mau_growth_60','paid_user_mau_growth_90','paid_user_mau_growth_180',
    'paid_user_util_growth_30','paid_user_util_growth_60','paid_user_util_growth_90','paid_user_util_growth_180','total_mthly_min_growth_30','total_mthly_min_growth_60',
    'total_mthly_min_growth_90','total_mthly_min_growth_180','total_mthly_mtgs_growth_30','total_mthly_mtgs_growth_60','total_mthly_mtgs_growth_90','total_mthly_mtgs_growth_180',
    'opp_amount','days_left_in_term','churn_downsell','churn_cancel','churn_gross','churn_downsell_next_90','churn_cancel_next_90','churn_gross_next_90'])


###bin data

currentterm_bins = [0,11,3000]
currentterm_bins_names = ['Month_Currentterm', 'Annual_Currentterm']
df['current_term'] = pd.cut(df['currentterm'], currentterm_bins, labels=currentterm_bins_names)
df = df.drop(['currentterm'],axis = 1)
print pd.value_counts(df['current_term'].values, sort=False)

mrr_bins = [0,14.99,100,1000,5000, 10000000000000]
mrr_group_names = ['MRR_14.99', 'MRR_100', 'MRR_1000', 'MRR_5000', 'MRR_5000+']
df['mrr_range'] = pd.cut(df['mrr_entry'], mrr_bins, labels=mrr_group_names)
df = df.drop(['mrr_entry'],axis = 1)
print pd.value_counts(df['mrr_range'].values, sort=False)

account_age_bins = [0,30,60,90,180,270,360,5000]
account_age_names = ['0-30 Days', '30-60 Days', '60-90 Days', '90-180 Days', '180-270 Days','270-360 Days','360+ Days']
df['account_age_range'] = pd.cut(df['account_age'], account_age_bins, labels=account_age_names)
df = df.drop(['account_age'],axis = 1)
print pd.value_counts(df['account_age_range'].values, sort=False)

###drop irrelevant fields

df = df.drop(['account_name', 'account_created_date','billtostate', 'billtocountry', 'churn_downsell', 'churn_cancel', 
              'churn_gross', 'churn_downsell_next_90', 'churn_cancel_next_90', 'mrr_exit', 'data_com_industry', 'free_mthly_min_growth_30', 
              'free_mthly_min_growth_60', 'free_mthly_min_growth_90', 'free_mthly_min_growth_180', 'free_mthly_min_per_mtg_growth_30', 'free_mthly_min_per_mtg_growth_60', 
              'free_mthly_min_per_mtg_growth_90', 'free_mthly_min_per_mtg_growth_180', 'free_mthly_min_per_user_growth_30', 'free_mthly_min_per_user_growth_60', 
              'free_mthly_min_per_user_growth_90', 'free_mthly_min_per_user_growth_180', 'free_mthly_mtgs_growth_30', 'free_mthly_mtgs_growth_60', 'free_mthly_mtgs_growth_90', 
              'free_mthly_mtgs_growth_180', 'free_mthly_mtgs_per_user_growth_30', 'free_mthly_mtgs_per_user_growth_60', 'free_mthly_mtgs_per_user_growth_90', 
              'free_mthly_mtgs_per_user_growth_180', 'free_user_count_growth_30', 'free_user_count_growth_60', 'free_user_count_growth_90', 'free_user_count_growth_180', 
              'free_user_mau_growth_30', 'free_user_mau_growth_60', 'free_user_mau_growth_90', 'free_user_mau_growth_180', 'free_user_util_growth_30', 'free_user_util_growth_60', 
              'free_user_util_growth_90', 'free_user_util_growth_180', 'free_meetings_last_30', 'free_minutes_last_30', 'free_meetings_per_user_last_30', 'free_minutes_per_user_last_30', 
              'free_minutes_per_meeting_last_30', 'zr_meetings_last_30', 'zr_minutes_last_30', 'zr_meetings_per_zr_last_30', 'zr_minutes_per_zr_last_30', 'zr_minutes_per_meeting_last_30',
              'daily_zr_meeting_attach_rate', 'daily_zr_minutes_attach_rate', 'webinar_meetings_last_30', 'webinar_minutes_last_30', 'webinar_minutes_per_meeting_last_30'], axis=1)

###branding the columns

target = 'churn_gross_next_90'
IDcol = 'zoom_account_no'
ahs_date = 'ahs_date'
cat_cols = ['sales_group','employee_count','coreproduct', 'current_term','mrr_range','account_age_range']
predictors = [x for x in df.columns if x not in [target, IDcol, ahs_date]]

###label encoder for categorical features

for var in cat_cols:
 number = LabelEncoder()
 df[var] = number.fit_transform(df[var].astype('str'))

###train test split

train,test = train_test_split(df, test_size = 0.35)
print len(train)
print len(test)
print pd.value_counts(train['churn_gross_next_90'].values, sort=False)
print pd.value_counts(test['churn_gross_next_90'].values, sort=False)



###cross validation for parameter tuning

param_test1 = {'max_depth':range(10,41,10), 'min_samples_leaf':range(5,31,6)}
gsearch1 = GridSearchCV(estimator = GradientBoostingClassifier(learning_rate=0.1, 
                                                               n_estimators=50, 
                                                               min_samples_split=200,
                                                               subsample=0.85, 
                                                               random_state=11,
                                                               max_features=45), 
param_grid = param_test1, scoring='roc_auc',n_jobs=-1,iid=False, cv=10)
gsearch1.fit(train[predictors],train[target])
gsearch1.grid_scores_, gsearch1.best_params_, gsearch1.best_score_

param_test1 = {'max_features':range(15,46,10), 'min_samples_split':range(150,301,50)}
gsearch1 = GridSearchCV(estimator = GradientBoostingClassifier(learning_rate=0.05, 
                                                               n_estimators=50,
                                                               max_depth=40,
                                                               min_samples_leaf=10, 
                                                               subsample=0.85, 
                                                               random_state=11), 
param_grid = param_test1, scoring='roc_auc',n_jobs=-1,iid=False, cv=10)
gsearch1.fit(train[predictors],train[target])
gsearch1.grid_scores_, gsearch1.best_params_, gsearch1.best_score_


###define predictors
###define tuned model
###run modelfit

################
### model 1a ### 4/23
################

gbm_tuned_1 = GradientBoostingClassifier(learning_rate=0.05, 
                                         n_estimators=700,
                                         max_depth=40, 
                                         min_samples_split=150,
                                         min_samples_leaf=5, 
                                         subsample=0.85, 
                                         random_state=11, 
                                         max_features=45)
gbm_tuned_1.fit(train[predictors], train['churn_gross_next_90'])

################
### model 2a ###
################

gbm_tuned_1 = GradientBoostingClassifier(learning_rate=0.05, 
                                         n_estimators=700,
                                         max_depth=35, 
                                         min_samples_split=150,
                                         min_samples_leaf=10, 
                                         subsample=0.85, 
                                         random_state=11, 
                                         max_features=15)
gbm_tuned_1.fit(train[predictors], train['churn_gross_next_90'])

'''
4/20

>>> gbm_tuned_1 = GradientBoostingClassifier(learning_rate=0.05, 
...                                          n_estimators=700,
...                                          max_depth=30, 
...                                          min_samples_split=200,
...                                          min_samples_leaf=5, 
...                                          subsample=0.85, 
...                                          random_state=None, 
...                                          max_features=45)
>>> gbm_tuned_1.fit(train[predictors], train['churn_gross_next_90'])

Model Report on UNSEEN Test Dataset
Accuracy : 0.9827
AUC Score (Additional Test): 0.673229

Predicted       0     1     All
True                           
0          214237   830  215067
1            2971  1602    4573
All        217208  2432  219640


4/24

>>> gbm_tuned_1 = GradientBoostingClassifier(learning_rate=0.05, 
...                                          n_estimators=700,
...                                          max_depth=40, 
...                                          min_samples_split=150,
...                                          min_samples_leaf=5, 
...                                          subsample=0.85, 
...                                          random_state=11, 
...                                          max_features=45)
>>> gbm_tuned_1.fit(train[predictors], train['churn_gross_next_90'])

Model Report on UNSEEN Test Dataset
Accuracy : 0.9818
AUC Score (Additional Test): 0.666674

Predicted       0     1     All
True                           
0          214098   969  215067
1            3028  1545    4573
All        217126  2514  219640



TO TRY

mean: 0.99785, std: 0.00063, params: {'max_depth': 40, 'min_samples_leaf': 5}, 
mean: 0.99785, std: 0.00063, params: {'max_depth': 40, 'min_samples_leaf': 10}, 
mean: 0.99785, std: 0.00063, params: {'max_depth': 35, 'min_samples_leaf': 10}, 
mean: 0.99660, std: 0.00078, params: {'max_features': 15, 'min_samples_split': 150}, 
mean: 0.99700, std: 0.00081, params: {'max_features': 45, 'min_samples_split': 150}, 

'''

################
### model 3a ###
################



###Print model report: AUC and Accuracy and Crosstab

test['predicted_churn'] = gbm_tuned_1.predict(test[predictors])

print "\nModel Report on Test Dataset"
print "Accuracy : %.4g" % metrics.accuracy_score(test['churn_gross_next_90'], test['predicted_churn'])
print "AUC Score (Test): %f" % metrics.roc_auc_score(test['churn_gross_next_90'], test['predicted_churn'])

pd.crosstab(test['churn_gross_next_90'], test['predicted_churn'], rownames=['True'], colnames=['Predicted'], 
            margins=True)

feat_imp = pd.Series(gbm_tuned_1.feature_importances_, index=predictors).sort_values(ascending=True)
print feat_imp

###import data from additional test set

obj_add = s3.get_object(Bucket='zoom-datascience',Key = 'acct_health_score/' + 'tabledata/' + 'mm_add.csv')
file = obj_add["Body"].read()

df_add = pd.read_csv(io.BytesIO(file),names = ['ahs_date','zoom_account_no','account_name','account_created_date','account_age','sales_group','employee_count','coreproduct','currentterm',
    'data_com_industry','billtostate','billtocountry','mrr_entry','mrr_exit','mrr_growth_30','mrr_growth_60','mrr_growth_90','mrr_growth_180','billing_hosts',
    'paid_user_count','billing_hosts_utilization','paid_user_mau','paid_user_utilization','free_user_count','free_user_mau','free_user_utilization',
    'total_zr_deployed','total_meetings_last_30','total_minutes_last_30','meetings_per_user_last_30','minutes_per_user_last_30','total_minutes_per_meeting_last_30',
    'paid_meetings_last_30','paid_minutes_last_30','paid_meetings_per_user_last_30','paid_minutes_per_user_last_30','paid_minutes_per_meeting_last_30',
    'free_meetings_last_30','free_minutes_last_30','free_meetings_per_user_last_30','free_minutes_per_user_last_30','free_minutes_per_meeting_last_30',
    'zr_meetings_last_30','zr_minutes_last_30','zr_meetings_per_zr_last_30','zr_minutes_per_zr_last_30','zr_minutes_per_meeting_last_30','daily_zr_meeting_attach_rate',
    'daily_zr_minutes_attach_rate','webinar_meetings_last_30','webinar_minutes_last_30','webinar_minutes_per_meeting_last_30','unique_host_last_30days',
    'unique_logins_last_30days','billing_hosts_growth_30','billing_hosts_growth_60','billing_hosts_growth_90','billing_hosts_growth_180','billing_hosts_util_growth_30',
    'billing_hosts_util_growth_60','billing_hosts_util_growth_90','billing_hosts_util_growth_180','free_mthly_min_growth_30','free_mthly_min_growth_60',
    'free_mthly_min_growth_90','free_mthly_min_growth_180','free_mthly_min_per_mtg_growth_30','free_mthly_min_per_mtg_growth_60','free_mthly_min_per_mtg_growth_90',
    'free_mthly_min_per_mtg_growth_180','free_mthly_min_per_user_growth_30','free_mthly_min_per_user_growth_60','free_mthly_min_per_user_growth_90',
    'free_mthly_min_per_user_growth_180','free_mthly_mtgs_growth_30','free_mthly_mtgs_growth_60','free_mthly_mtgs_growth_90','free_mthly_mtgs_growth_180',
    'free_mthly_mtgs_per_user_growth_30','free_mthly_mtgs_per_user_growth_60','free_mthly_mtgs_per_user_growth_90','free_mthly_mtgs_per_user_growth_180',
    'free_user_count_growth_30','free_user_count_growth_60','free_user_count_growth_90','free_user_count_growth_180','free_user_mau_growth_30','free_user_mau_growth_60',
    'free_user_mau_growth_90','free_user_mau_growth_180','free_user_util_growth_30','free_user_util_growth_60','free_user_util_growth_90','free_user_util_growth_180',
    'mthly_min_per_mtg_growth_30','mthly_min_per_mtg_growth_60','mthly_min_per_mtg_growth_90','mthly_min_per_mtg_growth_180','mthly_min_per_user_growth_30',
    'mthly_min_per_user_growth_60','mthly_min_per_user_growth_90','mthly_min_per_user_growth_180','mthly_mtgs_per_user_growth_30','mthly_mtgs_per_user_growth_60',
    'mthly_mtgs_per_user_growth_90','mthly_mtgs_per_user_growth_180','paid_mthly_min_growth_30','paid_mthly_min_growth_60','paid_mthly_min_growth_90',
    'paid_mthly_min_growth_180','paid_mthly_min_per_mtg_growth_30','paid_mthly_min_per_mtg_growth_60','paid_mthly_min_per_mtg_growth_90','paid_mthly_min_per_mtg_growth_180',
    'paid_mthly_min_per_user_growth_30','paid_mthly_min_per_user_growth_60','paid_mthly_min_per_user_growth_90','paid_mthly_min_per_user_growth_180',
    'paid_mthly_mtgs_growth_30','paid_mthly_mtgs_growth_60','paid_mthly_mtgs_growth_90','paid_mthly_mtgs_growth_180','paid_mthly_mtgs_per_user_growth_30',
    'paid_mthly_mtgs_per_user_growth_60','paid_mthly_mtgs_per_user_growth_90','paid_mthly_mtgs_per_user_growth_180','paid_user_count_growth_30','paid_user_count_growth_60',
    'paid_user_count_growth_90','paid_user_count_growth_180','paid_user_mau_growth_30','paid_user_mau_growth_60','paid_user_mau_growth_90','paid_user_mau_growth_180',
    'paid_user_util_growth_30','paid_user_util_growth_60','paid_user_util_growth_90','paid_user_util_growth_180','total_mthly_min_growth_30','total_mthly_min_growth_60',
    'total_mthly_min_growth_90','total_mthly_min_growth_180','total_mthly_mtgs_growth_30','total_mthly_mtgs_growth_60','total_mthly_mtgs_growth_90','total_mthly_mtgs_growth_180',
    'opp_amount','days_left_in_term','churn_downsell','churn_cancel','churn_gross','churn_downsell_next_90','churn_cancel_next_90','churn_gross_next_90'])

print df_add.head()
print df_add.columns[df_add.isnull().any()].tolist()

###bin the data

currentterm_bins = [0,11,3000]
currentterm_bins_names = ['Month_Currentterm', 'Annual_Currentterm']
df_add['current_term'] = pd.cut(df_add['currentterm'], currentterm_bins, labels=currentterm_bins_names)
df_add = df_add.drop(['currentterm'],axis = 1)
print pd.value_counts(df_add['current_term'].values, sort=False)

mrr_bins = [0,14.99,100,1000,5000, 10000000000000]
mrr_group_names = ['MRR_14.99', 'MRR_100', 'MRR_1000', 'MRR_5000', 'MRR_5000+']
df_add['mrr_range'] = pd.cut(df_add['mrr_entry'], mrr_bins, labels=mrr_group_names)
df_add = df_add.drop(['mrr_entry'],axis = 1)
print pd.value_counts(df_add['mrr_range'].values, sort=False)

account_age_bins = [0,30,60,90,180,270,360,5000]
account_age_names = ['0-30 Days', '30-60 Days', '60-90 Days', '90-180 Days', '180-270 Days','270-360 Days','360+ Days']
df_add['account_age_range'] = pd.cut(df_add['account_age'], account_age_bins, labels=account_age_names)
df_add = df_add.drop(['account_age'],axis = 1)
print pd.value_counts(df_add['account_age_range'].values, sort=False)

###drop irrelevant fields

df_add = df_add.drop(['account_name', 'account_created_date','billtostate', 'billtocountry', 'churn_downsell', 'churn_cancel', 
              'churn_gross', 'churn_downsell_next_90', 'churn_cancel_next_90', 'mrr_exit', 'data_com_industry', 'free_mthly_min_growth_30', 
              'free_mthly_min_growth_60', 'free_mthly_min_growth_90', 'free_mthly_min_growth_180', 'free_mthly_min_per_mtg_growth_30', 'free_mthly_min_per_mtg_growth_60', 
              'free_mthly_min_per_mtg_growth_90', 'free_mthly_min_per_mtg_growth_180', 'free_mthly_min_per_user_growth_30', 'free_mthly_min_per_user_growth_60', 
              'free_mthly_min_per_user_growth_90', 'free_mthly_min_per_user_growth_180', 'free_mthly_mtgs_growth_30', 'free_mthly_mtgs_growth_60', 'free_mthly_mtgs_growth_90', 
              'free_mthly_mtgs_growth_180', 'free_mthly_mtgs_per_user_growth_30', 'free_mthly_mtgs_per_user_growth_60', 'free_mthly_mtgs_per_user_growth_90', 
              'free_mthly_mtgs_per_user_growth_180', 'free_user_count_growth_30', 'free_user_count_growth_60', 'free_user_count_growth_90', 'free_user_count_growth_180', 
              'free_user_mau_growth_30', 'free_user_mau_growth_60', 'free_user_mau_growth_90', 'free_user_mau_growth_180', 'free_user_util_growth_30', 'free_user_util_growth_60', 
              'free_user_util_growth_90', 'free_user_util_growth_180', 'free_meetings_last_30', 'free_minutes_last_30', 'free_meetings_per_user_last_30', 'free_minutes_per_user_last_30', 
              'free_minutes_per_meeting_last_30', 'zr_meetings_last_30', 'zr_minutes_last_30', 'zr_meetings_per_zr_last_30', 'zr_minutes_per_zr_last_30', 'zr_minutes_per_meeting_last_30',
              'daily_zr_meeting_attach_rate', 'daily_zr_minutes_attach_rate', 'webinar_meetings_last_30', 'webinar_minutes_last_30', 'webinar_minutes_per_meeting_last_30'], axis=1)

###branding the columns

target = 'churn_gross_next_90'
IDcol = 'zoom_account_no'
ahs_date = 'ahs_date'
cat_cols = ['sales_group','employee_count','coreproduct', 'current_term','mrr_range','account_age_range']
predictors = [x for x in df_add.columns if x not in [target, IDcol,ahs_date]]

###create label encoders for categorical features

for var in cat_cols:
 number = LabelEncoder()
 df_add[var] = number.fit_transform(df_add[var].astype('str'))

###Print model report: AUC and Accuracy and Crosstab

df_add['predicted_churn_add'] = gbm_tuned_1.predict(df_add[predictors])
predict_churn_prob = gbm_tuned_1.predict_proba(df_add[predictors])
df_add['predicted_churn_add_prob']=predict_churn_prob[:,1]


df_add['predicted_churn_add'] = gsearch1.predict(df_add[predictors])
predict_churn_prob = gsearch1.predict_proba(df_add[predictors])
df_add['predicted_churn_add_prob']=predict_churn_prob[:,1]


print "Model Report on UNSEEN Test Dataset"
print "Accuracy : %.4g" % metrics.accuracy_score(df_add['churn_gross_next_90'], df_add['predicted_churn_add'])
print "AUC Score (Additional Test): %f" % metrics.roc_auc_score(df_add['churn_gross_next_90'], df_add['predicted_churn_add'])

pd.crosstab(df_add['churn_gross_next_90'], df_add['predicted_churn_add'], rownames=['True'], colnames=['Predicted'], 
            margins=True)

feat_imp = pd.Series(gbm_tuned_1.feature_importances_, index=predictors).sort_values(ascending=True)
print feat_imp

###store the model in pickle

import pickle

filename = '/home/datascience/mm_plus_model_4_20.sav'

pickle.dump(gbm_tuned_1, open(filename, 'wb'))


#################################################
### to run the current day data through model####
#################################################

###32GB ec2 instance

ssh ec2-user@54.174.221.32

###64GB ec2 instance

ssh ec2-user@52.91.193.93

###run python in datascience instance

sudo su - datascience

python

###Import libraries again

import pandas as pd
import numpy as np
import boto3
import io
import pickle
from sklearn.ensemble import GradientBoostingClassifier 
from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation, metrics 
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score

#filename = '/home/datascience/ml_model/input_model/ahs_3_seg_model_new.sav'
feat_imp = pd.Series(gbm_tuned_1.feature_importances_).sort_values(ascending=True)
print feat_imp

###load the model from pickle

filename = '/home/datascience/ahs_6_segs_mm_plus.sav'

gbm_tuned_1 = pickle.load(open(filename, 'rb'))
#result = loaded_model.score(X_test, Y_test)
#print(result)

###load current day data

s3 = boto3.client('s3')
obj_today = s3.get_object(Bucket='zoom-datascience',Key = 'acct_health_score/' + 'dailyInputData/' + 'larger_20170328.csv')
file = obj_today["Body"].read()

df_today = pd.read_csv(io.BytesIO(file),names = ['ahs_date','zoom_account_no','account_name','account_created_date','account_age','sales_group',
  'employee_count','coreproduct','currentterm','data_com_industry','billtostate','billtocountry','mrr_entry','mrr_exit','mrr_growth_30',
  'mrr_growth_60','mrr_growth_90','mrr_growth_180','billing_hosts','paid_user_count','billing_hosts_utilization','paid_user_mau','paid_user_utilization',
  'free_user_count','free_user_mau','free_user_utilization','total_zr_deployed','total_meetings_last_30','total_minutes_last_30','meetings_per_user_last_30',
  'minutes_per_user_last_30','total_minutes_per_meeting_last_30','paid_meetings_last_30','paid_minutes_last_30','paid_meetings_per_user_last_30',
  'paid_minutes_per_user_last_30','paid_minutes_per_meeting_last_30','free_meetings_last_30','free_minutes_last_30','free_meetings_per_user_last_30',
  'free_minutes_per_user_last_30','free_minutes_per_meeting_last_30','zr_meetings_last_30','zr_minutes_last_30','zr_meetings_per_zr_last_30','zr_minutes_per_zr_last_30',
  'zr_minutes_per_meeting_last_30','daily_zr_meeting_attach_rate','daily_zr_minutes_attach_rate','webinar_meetings_last_30','webinar_minutes_last_30',
  'webinar_minutes_per_meeting_last_30','unique_host_last_30days','unique_logins_last_30days','billing_hosts_growth_30','billing_hosts_growth_60','billing_hosts_growth_90',
  'billing_hosts_growth_180','billing_hosts_util_growth_30','billing_hosts_util_growth_60','billing_hosts_util_growth_90','billing_hosts_util_growth_180',
  'free_mthly_min_growth_30','free_mthly_min_growth_60','free_mthly_min_growth_90','free_mthly_min_growth_180','free_mthly_min_per_mtg_growth_30',
  'free_mthly_min_per_mtg_growth_60','free_mthly_min_per_mtg_growth_90','free_mthly_min_per_mtg_growth_180','free_mthly_min_per_user_growth_30',
  'free_mthly_min_per_user_growth_60','free_mthly_min_per_user_growth_90','free_mthly_min_per_user_growth_180','free_mthly_mtgs_growth_30','free_mthly_mtgs_growth_60',
  'free_mthly_mtgs_growth_90','free_mthly_mtgs_growth_180','free_mthly_mtgs_per_user_growth_30','free_mthly_mtgs_per_user_growth_60','free_mthly_mtgs_per_user_growth_90',
  'free_mthly_mtgs_per_user_growth_180','free_user_count_growth_30','free_user_count_growth_60','free_user_count_growth_90','free_user_count_growth_180','free_user_mau_growth_30',
  'free_user_mau_growth_60','free_user_mau_growth_90','free_user_mau_growth_180','free_user_util_growth_30','free_user_util_growth_60','free_user_util_growth_90',
  'free_user_util_growth_180','mthly_min_per_mtg_growth_30','mthly_min_per_mtg_growth_60','mthly_min_per_mtg_growth_90','mthly_min_per_mtg_growth_180','mthly_min_per_user_growth_30',
  'mthly_min_per_user_growth_60','mthly_min_per_user_growth_90','mthly_min_per_user_growth_180','mthly_mtgs_per_user_growth_30','mthly_mtgs_per_user_growth_60',
  'mthly_mtgs_per_user_growth_90','mthly_mtgs_per_user_growth_180','paid_mthly_min_growth_30','paid_mthly_min_growth_60','paid_mthly_min_growth_90','paid_mthly_min_growth_180',
  'paid_mthly_min_per_mtg_growth_30','paid_mthly_min_per_mtg_growth_60','paid_mthly_min_per_mtg_growth_90','paid_mthly_min_per_mtg_growth_180','paid_mthly_min_per_user_growth_30',
  'paid_mthly_min_per_user_growth_60','paid_mthly_min_per_user_growth_90','paid_mthly_min_per_user_growth_180','paid_mthly_mtgs_growth_30','paid_mthly_mtgs_growth_60',
  'paid_mthly_mtgs_growth_90','paid_mthly_mtgs_growth_180','paid_mthly_mtgs_per_user_growth_30','paid_mthly_mtgs_per_user_growth_60','paid_mthly_mtgs_per_user_growth_90',
  'paid_mthly_mtgs_per_user_growth_180','paid_user_count_growth_30','paid_user_count_growth_60','paid_user_count_growth_90','paid_user_count_growth_180','paid_user_mau_growth_30',
  'paid_user_mau_growth_60','paid_user_mau_growth_90','paid_user_mau_growth_180','paid_user_util_growth_30','paid_user_util_growth_60','paid_user_util_growth_90','paid_user_util_growth_180',
  'total_mthly_min_growth_30','total_mthly_min_growth_60','total_mthly_min_growth_90','total_mthly_min_growth_180','total_mthly_mtgs_growth_30','total_mthly_mtgs_growth_60',
  'total_mthly_mtgs_growth_90','total_mthly_mtgs_growth_180','opp_amount','days_left_in_term','churn_downsell','churn_cancel','churn_gross','churn_downsell_next_90','churn_cancel_next_90','churn_gross_next_90'])

#print df_today.head()
#print df_today.columns[df_today.isnull().any()].tolist()

###bin the data

currentterm_bins = [0,11,3000]
currentterm_bins_names = ['Month_Currentterm', 'Annual_Currentterm']
df_today['current_term'] = pd.cut(df_today['currentterm'], currentterm_bins, labels=currentterm_bins_names)
df_today = df_today.drop(['currentterm'],axis = 1)
print pd.value_counts(df_today['current_term'].values, sort=False)

mrr_bins = [0,14.99,100,1000,5000, 10000000000000]
mrr_group_names = ['MRR_14.99', 'MRR_100', 'MRR_1000', 'MRR_5000', 'MRR_5000+']
df_today['mrr_range'] = pd.cut(df_today['mrr_entry'], mrr_bins, labels=mrr_group_names)
df_today = df_today.drop(['mrr_entry'],axis = 1)
print pd.value_counts(df_today['mrr_range'].values, sort=False)

account_age_bins = [0,30,60,90,180,270,360,5000]
account_age_names = ['0-30 Days', '30-60 Days', '60-90 Days', '90-180 Days', '180-270 Days','270-360 Days','360+ Days']
df_today['account_age_range'] = pd.cut(df_today['account_age'], account_age_bins, labels=account_age_names)
df_today = df_today.drop(['account_age'],axis = 1)
print pd.value_counts(df_today['account_age_range'].values, sort=False)

###drop irrelevant fields

df_today = df_today.drop(['account_name', 'account_created_date','billtostate', 'billtocountry', 'churn_downsell', 'churn_cancel', 
              'churn_gross', 'churn_downsell_next_90', 'churn_cancel_next_90', 'mrr_exit', 'data_com_industry', 'free_mthly_min_growth_30', 
              'free_mthly_min_growth_60', 'free_mthly_min_growth_90', 'free_mthly_min_growth_180', 'free_mthly_min_per_mtg_growth_30', 'free_mthly_min_per_mtg_growth_60', 
              'free_mthly_min_per_mtg_growth_90', 'free_mthly_min_per_mtg_growth_180', 'free_mthly_min_per_user_growth_30', 'free_mthly_min_per_user_growth_60', 
              'free_mthly_min_per_user_growth_90', 'free_mthly_min_per_user_growth_180', 'free_mthly_mtgs_growth_30', 'free_mthly_mtgs_growth_60', 'free_mthly_mtgs_growth_90', 
              'free_mthly_mtgs_growth_180', 'free_mthly_mtgs_per_user_growth_30', 'free_mthly_mtgs_per_user_growth_60', 'free_mthly_mtgs_per_user_growth_90', 
              'free_mthly_mtgs_per_user_growth_180', 'free_user_count_growth_30', 'free_user_count_growth_60', 'free_user_count_growth_90', 'free_user_count_growth_180', 
              'free_user_mau_growth_30', 'free_user_mau_growth_60', 'free_user_mau_growth_90', 'free_user_mau_growth_180', 'free_user_util_growth_30', 'free_user_util_growth_60', 
              'free_user_util_growth_90', 'free_user_util_growth_180', 'free_meetings_last_30', 'free_minutes_last_30', 'free_meetings_per_user_last_30', 'free_minutes_per_user_last_30', 
              'free_minutes_per_meeting_last_30', 'zr_meetings_last_30', 'zr_minutes_last_30', 'zr_meetings_per_zr_last_30', 'zr_minutes_per_zr_last_30', 'zr_minutes_per_meeting_last_30',
              'daily_zr_meeting_attach_rate', 'daily_zr_minutes_attach_rate', 'webinar_meetings_last_30', 'webinar_minutes_last_30', 'webinar_minutes_per_meeting_last_30'], axis=1)

###branding the columns

target = 'churn_gross_next_90'
IDcol = ['ahs_date','zoom_account_no']
cat_cols = ['sales_group','employee_count','coreproduct', 'current_term','mrr_range','account_age_range']

###create label encoders for categorical features

for var in cat_cols:
 number = LabelEncoder()
 df_today[var] = number.fit_transform(df_today[var].astype('str'))

###generate predicted results to EC2 box

predictors = [x for x in df_today.columns if x not in [target, IDcol]]

y_predict_proba = gbm_tuned_1.predict_proba(df_today[predictors])

df_today['Prediction'] = gbm_tuned_1.predict(df_today[predictors])

labels2idx = {label: i for i, label in enumerate(gbm_tuned_1.classes_)}
for label in [0,1]:
    df_today[label] = y_predict_proba[:, labels2idx[label]]
    df_today = df_today.rename(columns = {1:'Churn_Probability', 0:'Non_Churn_Probability'})

#predict_churn_prob = gbm_tuned_1.predict_proba(df_today[predictors])
#df_today['predicted_churn_today_prob']=predict_churn_prob[:,1]

df_today.to_csv('/home/datascience/mm_plus_output.csv')

###download results from EC2 box s3

data = open('/home/datascience/mm_plus_output.csv', 'rb')
s3.put_object(Bucket='zoom-datascience', Key = 'acct_health_score/' + 'dailyOutputData/' + 'mm_plus_output.csv', Body=data)




