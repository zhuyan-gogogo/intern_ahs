import io
import boto3
import pickle
import pandas as pd
import numpy as np
import ProcessConstantsDS
import psycopg2
from datetime import datetime, date, timedelta
from sklearn import cross_validation, metrics
from sklearn.grid_search import GridSearchCV
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import cross_validation, metrics
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score


conn = psycopg2.connect(
    host=ProcessConstantsDS.host,
    user=ProcessConstantsDS.user,
    port=ProcessConstantsDS.port,
    password=ProcessConstantsDS.password,
    dbname=ProcessConstantsDS.dbname)
print("Database Connection Successfull")
cur = conn.cursor()

now= datetime.today() - timedelta(days=2)
dt = now.strftime("%Y-%m-%d")

df_today = pd.read_csv('/home/datascience/ml_model/inputData/test_data_large.csv')

###bin the data

currentterm_bins = [0,11,3000]
currentterm_bins_names = ['Month_Currentterm', 'Annual_Currentterm']
df_today['current_term'] = pd.cut(df_today['currentterm'], currentterm_bins, labels=currentterm_bins_names)
df_today = df_today.drop(['currentterm'],axis = 1)
#print(pd.value_counts(df_today['current_term'].values, sort=False))

mrr_bins = [0,14.99,100,1000,5000, 10000000000000]
mrr_group_names = ['MRR_14.99', 'MRR_100', 'MRR_1000', 'MRR_5000', 'MRR_5000+']
df_today['mrr_range'] = pd.cut(df_today['mrr_entry'], mrr_bins, labels=mrr_group_names)
df_today = df_today.drop(['mrr_entry'],axis = 1)
#print(pd.value_counts(df_today['mrr_range'].values, sort=False))

account_age_bins = [0,30,60,90,180,270,360,5000]
account_age_names = ['0-30 Days', '30-60 Days', '60-90 Days', '90-180 Days', '180-270 Days','270-360 Days','360+ Days']
df_today['account_age_range'] = pd.cut(df_today['account_age'], account_age_bins, labels=account_age_names)
df_today = df_today.drop(['account_age'],axis = 1)
#print(pd.value_counts(df_today['account_age_range'].values, sort=False))

###drop irrelevant fields

df_today = df_today.drop(['account_name', 'account_created_date','billtostate', 'billtocountry', 'churn_downsell', 'churn_cancel', 
              'churn_gross', 'churn_downsell_next_90', 'churn_cancel_next_90', 'mrr_exit', 'data_com_industry', 'free_mthly_min_growth_30', 
              'free_mthly_min_growth_60', 'free_mthly_min_growth_90', 'free_mthly_min_growth_180', 'free_mthly_min_per_mtg_growth_30', 'free_mthly_min_per_mtg_growth_60', 
              'free_mthly_min_per_mtg_growth_90', 'free_mthly_min_per_mtg_growth_180', 'free_mthly_min_per_user_growth_30', 'free_mthly_min_per_user_growth_60', 
              'free_mthly_min_per_user_growth_90', 'free_mthly_min_per_user_growth_180', 'free_mthly_mtgs_growth_30', 'free_mthly_mtgs_growth_60', 'free_mthly_mtgs_growth_90', 
              'free_mthly_mtgs_growth_180', 'free_mthly_mtgs_per_user_growth_30', 'free_mthly_mtgs_per_user_growth_60', 'free_mthly_mtgs_per_user_growth_90', 
              'free_mthly_mtgs_per_user_growth_180', 'free_user_count_growth_30', 'free_user_count_growth_60', 'free_user_count_growth_90', 'free_user_count_growth_180', 
              'free_user_mau_growth_30', 'free_user_mau_growth_60', 'free_user_mau_growth_90', 'free_user_mau_growth_180', 'free_user_util_growth_30', 'free_user_util_growth_60', 
              'free_user_util_growth_90', 'free_user_util_growth_180', 'free_meetings_last_30', 'free_minutes_last_30', 'free_meetings_per_user_last_30', 'free_minutes_per_user_last_30', 
              'free_minutes_per_meeting_last_30', 'zr_meetings_last_30', 'zr_minutes_last_30', 'zr_meetings_per_zr_last_30', 'zr_minutes_per_zr_last_30', 'zr_minutes_per_meeting_last_30',
              'daily_zr_meeting_attach_rate', 'daily_zr_minutes_attach_rate', 'webinar_meetings_last_30', 'webinar_minutes_last_30', 'webinar_minutes_per_meeting_last_30'], axis=1)

###branding the columns

target = 'churn_gross_next_90'
IDcol = 'zoom_account_no'
ahs_date = 'ahs_date'
cat_cols = ['sales_group','employee_count','coreproduct', 'current_term','mrr_range','account_age_range']

###create label encoders for categorical features

for var in cat_cols:
 number = LabelEncoder()
 df_today[var] = number.fit_transform(df_today[var].astype('str'))

###Import model

filename = '/home/datascience/ml_model/input_model/ahs_mm_plus_model_4_20.sav'
### filename = '/home/datascience/ahs_6_segs_mm_plus.sav'

gbm_load_large = pickle.load(open(filename, 'rb'))

###generate predicted results to EC2 box

predictors = [x for x in df_today.columns if x not in [target, IDcol, ahs_date]]

y_predict_proba = gbm_load_large.predict_proba(df_today[predictors])

df_today['Prediction'] = gbm_load_large.predict(df_today[predictors])

labels2idx = {label: i for i, label in enumerate(gbm_load_large.classes_)}
for label in [0,1]:
    df_today[label] = y_predict_proba[:, labels2idx[label]]
    df_today = df_today.rename(columns = {1:'Churn_Probability', 0:'Non_Churn_Probability'})

df_today.to_csv('/home/datascience/ml_model/outputData/ahs_mm_plus_model_output.csv', columns=['ahs_date','zoom_account_no','Non_Churn_Probability','Churn_Probability','Prediction'], index=False)

s3 = boto3.resource('s3')
data = open('/home/datascience/ml_model/outputData/ahs_mm_plus_model_output.csv', 'rb')
s3.Bucket('zoom-datascience').put_object(Key='acct_health_score/' + 'dailyOutputData/' + dt+ '/' + 'ahs_mm_plus_model_output.csv', Body=data)

copy_query = """copy etl_acct_health.ahs_predictproba
from 's3://zoom-datascience/acct_health_score/dailyOutputData/%s/ahs_mm_plus_model_output.csv'
credentials 'aws_access_key_id=AKIAIA77SPMP2QXKPCSQ;aws_secret_access_key=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'
csv
IGNOREHEADER 1"""%(dt)
cur.execute(copy_query)
conn.commit()



