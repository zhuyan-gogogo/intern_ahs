###64GB ec2 instance
#ssh ec2-user@52.91.193.93

###32GB ec2 instance
#ssh ec2-user@54.174.221.32

###run python in datascience instance
#sudo su - datascience
#python

import io
import boto3
import pickle
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import cross_validation, metrics
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score

# s3 = boto3.client('s3')
# # Replace the path of the file name as per requirement
# obj = s3.get_object(Bucket='zoom-datascience',Key = 'acct_health_score/' + 'tabledata/' + 'febmarch.csv')
# train_file = obj["Body"].read()

train_file = '../data/JanFeb.csv'

df = pd.read_csv(train_file, sep='\t')
# df.info()
# 154 columns

df = df.drop(['ahs_date', 'account_name', 'account_created_date','billtostate', 'billtocountry', 'mrr_exit', 'churn_downsell', 'churn_cancel', 'churn_gross', 'churn_downsell_next_90', 'churn_cancel_next_90', 'zr_meetings_last_30','zr_minutes_last_30','zr_meetings_per_zr_last_30','zr_minutes_per_zr_last_30','zr_minutes_per_meeting_last_30','daily_zr_meeting_attach_rate','daily_zr_minutes_attach_rate','webinar_meetings_last_30','webinar_minutes_last_30','webinar_minutes_per_meeting_last_30','free_mthly_min_growth_30','free_mthly_min_growth_60','free_mthly_min_growth_90','free_mthly_min_growth_180','free_mthly_min_per_mtg_growth_30','free_mthly_min_per_mtg_growth_60','free_mthly_min_per_mtg_growth_90','free_mthly_min_per_mtg_growth_180','free_mthly_min_per_user_growth_30','free_mthly_min_per_user_growth_60','free_mthly_min_per_user_growth_90','free_mthly_min_per_user_growth_180','free_mthly_mtgs_growth_30','free_mthly_mtgs_growth_60','free_mthly_mtgs_growth_90','free_mthly_mtgs_growth_180','free_mthly_mtgs_per_user_growth_30','free_mthly_mtgs_per_user_growth_60','free_mthly_mtgs_per_user_growth_90','free_mthly_mtgs_per_user_growth_180','free_user_count_growth_30','free_user_count_growth_60','free_user_count_growth_90','free_user_count_growth_180','free_user_mau_growth_30','free_user_mau_growth_60','free_user_mau_growth_90','free_user_mau_growth_180','free_user_util_growth_30','free_user_util_growth_60','free_user_util_growth_90','free_user_util_growth_180'], axis=1)
# 101 columns

# check any columns have missing data
df.columns[df.isnull().any()].tolist()

#segment terms
currentterm_bins = [0,11,3000]
currentterm_bins_names = ['Month_Currentterm', 'Annual_Currentterm']
df['current_term'] = pd.cut(df['currentterm'], currentterm_bins, labels=currentterm_bins_names)

mrr_bins = [0,14.99,100,10000000000000]
mrr_group_names = ['MRR_14.99', 'MRR_100', 'MRR_100+']
df['mrr_entry_range'] = pd.cut(df['mrr_entry'], mrr_bins, labels=mrr_group_names)

account_age_bins = [0,30,60,90,180,270,360,5000]
account_age_names = ['0-30 Days', '30-60 Days', '60-90 Days', '90-180 Days', '180-270 Days','270-360 Days','360+ Days']
df['account_age_range'] = pd.cut(df['account_age'], account_age_bins, labels=account_age_names)

days_left_in_term = [0,30,45,60,90,120,180,270,365,720,50000]
days_left_in_term_names = ['0-30 Days', '30-45 Days', '45-60 Days', '60-90 Days', '90-120 Days','120-180 Days','180-270 Days', '270-365 Days','2 years','more than 2 years']
df['days_left_in_term_range'] = pd.cut(df['days_left_in_term'], days_left_in_term, labels=days_left_in_term_names)

df = df.drop(['currentterm', 'mrr_entry', 'account_age','days_left_in_term'], axis=1)

target = 'churn_gross_next_90'
IDcol = 'zoom_account_no'
cat_cols = ['sales_group','employee_count','data_com_industry','coreproduct', 'current_term','mrr_entry_range','account_age_range','days_left_in_term_range']

for var in cat_cols:
 number = LabelEncoder()
 df[var] = number.fit_transform(df[var].astype('str'))

train, test = train_test_split(df, test_size = 0.35)
pd.value_counts(train['churn_gross_next_90'].values, sort=False)
pd.value_counts(test['churn_gross_next_90'].values, sort=False)


predictors = [x for x in train.columns if x not in [target, IDcol]]
gbm_tuned = GradientBoostingClassifier(learning_rate=0.075, 
                                         n_estimators=700,
                                         max_depth=45, 
                                         min_samples_split=150,
                                         min_samples_leaf=20, 
                                         subsample=0.85, 
                                         random_state=10, 
                                         max_features=45)
ahs_3_seg_model= gbm_tuned.fit(train[predictors], train['churn_gross_next_90'])


# checking the accuracy of the model
y_pred = ahs_3_seg_model.predict(test[predictors])

print "\nModel Report on Test Dataset"
print "Accuracy : %.4g" % metrics.accuracy_score(test['churn_gross_next_90'], y_pred)
print "AUC Score (Test): %f" % metrics.roc_auc_score(test['churn_gross_next_90'], y_pred)

print pd.crosstab(test['churn_gross_next_90'], y_pred, rownames=['True'], colnames=['Predicted'], margins=True)

