/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install gcc --without-multilib
which gcc

cd <directory>/xgboost

vim make/config.mk

export CC = gcc-6
export CXX = g++-6

cp make/config.mk .
make -j4