import io
import boto3
import pickle
import pandas as pd
import numpy as np
import ProcessConstantsDS
import psycopg2
from datetime import datetime, date, timedelta
from sklearn import cross_validation, metrics
from sklearn.grid_search import GridSearchCV
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import cross_validation, metrics
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score


conn = psycopg2.connect(
    host=ProcessConstantsDS.host,
    user=ProcessConstantsDS.user,
    port=ProcessConstantsDS.port,
    password=ProcessConstantsDS.password,
    dbname=ProcessConstantsDS.dbname)
print("Database Connection Successfull")
cur = conn.cursor()


now= datetime.today() - timedelta(days=2)
dt = now.strftime("%Y-%m-%d")

test_df = pd.read_csv('/home/datascience/ml_model/inputData/test_data_small.csv')

test_df = test_df.drop(['account_name', 'account_created_date','billtostate', 'billtocountry', 'mrr_exit', 'churn_downsell', 'churn_cancel', 'churn_gross', 'churn_downsell_next_90', 'churn_cancel_next_90', 'zr_meetings_last_30','zr_minutes_last_30','zr_meetings_per_zr_last_30','zr_minutes_per_zr_last_30','zr_minutes_per_meeting_last_30','daily_zr_meeting_attach_rate','daily_zr_minutes_attach_rate','webinar_meetings_last_30','webinar_minutes_last_30','webinar_minutes_per_meeting_last_30','free_mthly_min_growth_30','free_mthly_min_growth_60','free_mthly_min_growth_90','free_mthly_min_growth_180','free_mthly_min_per_mtg_growth_30','free_mthly_min_per_mtg_growth_60','free_mthly_min_per_mtg_growth_90','free_mthly_min_per_mtg_growth_180','free_mthly_min_per_user_growth_30','free_mthly_min_per_user_growth_60','free_mthly_min_per_user_growth_90','free_mthly_min_per_user_growth_180','free_mthly_mtgs_growth_30','free_mthly_mtgs_growth_60','free_mthly_mtgs_growth_90','free_mthly_mtgs_growth_180','free_mthly_mtgs_per_user_growth_30','free_mthly_mtgs_per_user_growth_60','free_mthly_mtgs_per_user_growth_90','free_mthly_mtgs_per_user_growth_180','free_user_count_growth_30','free_user_count_growth_60','free_user_count_growth_90','free_user_count_growth_180','free_user_mau_growth_30','free_user_mau_growth_60','free_user_mau_growth_90','free_user_mau_growth_180','free_user_util_growth_30','free_user_util_growth_60','free_user_util_growth_90','free_user_util_growth_180'], axis=1)

test_df.columns[test_df.isnull().any()].tolist()

currentterm_bins = [0,11,3000]
currentterm_bins_names = ['Month_Currentterm', 'Annual_Currentterm']
test_df['current_term'] = pd.cut(test_df['currentterm'], currentterm_bins, labels=currentterm_bins_names)

mrr_bins = [0,14.99,100,10000000000000]
mrr_group_names = ['MRR_14.99', 'MRR_100', 'MRR_100+']
test_df['mrr_entry_range'] = pd.cut(test_df['mrr_entry'], mrr_bins, labels=mrr_group_names)

account_age_bins = [0,30,60,90,180,270,360,5000]
account_age_names = ['0-30 Days', '30-60 Days', '60-90 Days', '90-180 Days', '180-270 Days','270-360 Days','360+ Days']
test_df['account_age_range'] = pd.cut(test_df['account_age'], account_age_bins, labels=account_age_names)

days_left_in_term = [0,30,45,60,90,120,180,270,365,720,50000]
days_left_in_term_names = ['0-30 Days', '30-45 Days', '45-60 Days', '60-90 Days', '90-120 Days','120-180 Days','180-270 Days', '270-365 Days','2 years','more than 2 years']
test_df['days_left_in_term_range'] = pd.cut(test_df['days_left_in_term'], days_left_in_term, labels=days_left_in_term_names)

test_df = test_df.drop(['currentterm', 'mrr_entry', 'account_age','days_left_in_term'], axis=1)

target = 'churn_gross_next_90'
IDcol = 'zoom_account_no'
typecol = 'Type'
ahs_date = 'ahs_date'
cat_cols = ['sales_group','employee_count','data_com_industry','coreproduct', 'current_term','mrr_entry_range','account_age_range','days_left_in_term_range']

for var in cat_cols:
 number = LabelEncoder()
 test_df[var] = number.fit_transform(test_df[var].astype('str'))


ahs_3_seg_model_file_new = '/home/datascience/ml_model/input_model/ahs_sij_model_4_25.sav'
loaded_ahs_model = pickle.load(open(ahs_3_seg_model_file_new, 'rb'))

predictors = [x for x in test_df.columns if x not in [target, IDcol, typecol, ahs_date]]

y_predict_proba = loaded_ahs_model.predict_proba(test_df[predictors])

labels2idx = {label: i for i, label in enumerate(loaded_ahs_model.classes_)}
for label in [0,1]:
    test_df[label] = y_predict_proba[:, labels2idx[label]]

test_df = test_df.rename(columns = {1:'Churn_Probability', 0:'Non_Churn_Probability'})

test_df['Prediction']= loaded_ahs_model.predict(test_df[predictors])


test_df.to_csv('/home/datascience/ml_model/outputData/ahs_sij_model_output.csv',columns=['ahs_date','zoom_account_no','Non_Churn_Probability','Churn_Probability','Prediction'],index=False)

# only for s3
s3 = boto3.resource('s3')
data = open('/home/datascience/ml_model/outputData/ahs_sij_model_output.csv', 'rb')
s3.Bucket('zoom-datascience').put_object(Key='acct_health_score/' + 'dailyOutputData/' + dt+ '/' + 'ahs_sij_model_output.csv', Body=data)

copy_query = """copy etl_acct_health.ahs_predictproba
from 's3://zoom-datascience/acct_health_score/dailyOutputData/%s/ahs_sij_model_output.csv'
credentials 'aws_access_key_id=AKIAIA77SPMP2QXKPCSQ;aws_secret_access_key=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'
csv
IGNOREHEADER 1"""%(dt)
cur.execute(copy_query)
conn.commit()