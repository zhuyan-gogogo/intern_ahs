import io
import pandas as pd
import numpy as np
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score
import boto3

# ---------- ---------- ---------- ---------- ---------- load data
train_file = '../data/JanFeb.csv'

train = pd.read_csv(train_file, sep='\t')


def feature_engineer(df, group='sij'):
    '''
    from a dataframe, deploy a set of feature engineering method, return a new set a features
    :param df: 
    :param dropOption: sij: drop 53 features, mm: 58 features 
    :return: df
    '''

    currentterm_bins = [0, 11, 3000]
    currentterm_bins_names = ['Month_Currentterm', 'Annual_Currentterm']
    df['current_term'] = pd.cut(df['currentterm'], currentterm_bins, labels=currentterm_bins_names)

    mrr_bins = [0, 14.99, 100, 10000000000000]
    mrr_group_names = ['MRR_14.99', 'MRR_100', 'MRR_100+']
    df['mrr_entry_range'] = pd.cut(df['mrr_entry'], mrr_bins, labels=mrr_group_names)

    account_age_bins = [0, 30, 60, 90, 180, 270, 360, 5000]
    account_age_names = ['0-30 Days', '30-60 Days', '60-90 Days', '90-180 Days', '180-270 Days', '270-360 Days',
                         '360+ Days']
    df['account_age_range'] = pd.cut(df['account_age'], account_age_bins, labels=account_age_names)

    days_left_in_term = [0, 30, 45, 60, 90, 120, 180, 270, 365, 720, 50000]
    days_left_in_term_names = ['0-30 Days', '30-45 Days', '45-60 Days', '60-90 Days', '90-120 Days', '120-180 Days',
                               '180-270 Days', '270-365 Days', '2 years', 'more than 2 years']
    df['days_left_in_term_range'] = pd.cut(df['days_left_in_term'], days_left_in_term, labels=days_left_in_term_names)

    df = df.drop(['currentterm', 'mrr_entry', 'account_age', 'days_left_in_term'], axis=1)
    print pd.value_counts(df['account_age_range'].values, sort=False)

    ###drop irrelevant fields

    if group == 'sij':
        # usually for
        cols = ['ahs_date',
                'account_name',
                'account_created_date',
                'billtostate',
                'billtocountry',
                'mrr_exit',
                'churn_downsell',
                'churn_cancel',
                'churn_gross',
                'churn_downsell_next_90',
                'churn_cancel_next_90',
                'zr_meetings_last_30',
                'zr_minutes_last_30',
                'zr_meetings_per_zr_last_30',
                'zr_minutes_per_zr_last_30',
                'zr_minutes_per_meeting_last_30',
                'daily_zr_meeting_attach_rate',
                'daily_zr_minutes_attach_rate',
                'webinar_meetings_last_30',
                'webinar_minutes_last_30',
                'webinar_minutes_per_meeting_last_30',
                'free_mthly_min_growth_30',
                'free_mthly_min_growth_60',
                'free_mthly_min_growth_90',
                'free_mthly_min_growth_180',
                'free_mthly_min_per_mtg_growth_30',
                'free_mthly_min_per_mtg_growth_60',
                'free_mthly_min_per_mtg_growth_90',
                'free_mthly_min_per_mtg_growth_180',
                'free_mthly_min_per_user_growth_30',
                'free_mthly_min_per_user_growth_60',
                'free_mthly_min_per_user_growth_90',
                'free_mthly_min_per_user_growth_180',
                'free_mthly_mtgs_growth_30',
                'free_mthly_mtgs_growth_60',
                'free_mthly_mtgs_growth_90',
                'free_mthly_mtgs_growth_180',
                'free_mthly_mtgs_per_user_growth_30',
                'free_mthly_mtgs_per_user_growth_60',
                'free_mthly_mtgs_per_user_growth_90',
                'free_mthly_mtgs_per_user_growth_180',
                'free_user_count_growth_30',
                'free_user_count_growth_60',
                'free_user_count_growth_90',
                'free_user_count_growth_180',
                'free_user_mau_growth_30',
                'free_user_mau_growth_60',
                'free_user_mau_growth_90',
                'free_user_mau_growth_180',
                'free_user_util_growth_30',
                'free_user_util_growth_60',
                'free_user_util_growth_90',
                'free_user_util_growth_180']
        cat_cols = ['sales_group', 'employee_count', 'data_com_industry', 'coreproduct', 'current_term',
                    'mrr_entry_range', 'account_age_range', 'days_left_in_term_range']


    elif group == 'mm':
        cols = ['ahs_date','account_name',
                'account_created_date',
                'billtostate',
                'billtocountry',
                'churn_downsell',
                'churn_cancel',
                'churn_gross',
                'churn_downsell_next_90',
                'churn_cancel_next_90',
                'mrr_exit',
                'data_com_industry',
                'free_mthly_min_growth_30',
                'free_mthly_min_growth_60',
                'free_mthly_min_growth_90',
                'free_mthly_min_growth_180',
                'free_mthly_min_per_mtg_growth_30',
                'free_mthly_min_per_mtg_growth_60',
                'free_mthly_min_per_mtg_growth_90',
                'free_mthly_min_per_mtg_growth_180',
                'free_mthly_min_per_user_growth_30',
                'free_mthly_min_per_user_growth_60',
                'free_mthly_min_per_user_growth_90',
                'free_mthly_min_per_user_growth_180',
                'free_mthly_mtgs_growth_30',
                'free_mthly_mtgs_growth_60',
                'free_mthly_mtgs_growth_90',
                'free_mthly_mtgs_growth_180',
                'free_mthly_mtgs_per_user_growth_30',
                'free_mthly_mtgs_per_user_growth_60',
                'free_mthly_mtgs_per_user_growth_90',
                'free_mthly_mtgs_per_user_growth_180',
                'free_user_count_growth_30',
                'free_user_count_growth_60',
                'free_user_count_growth_90',
                'free_user_count_growth_180',
                'free_user_mau_growth_30',
                'free_user_mau_growth_60',
                'free_user_mau_growth_90',
                'free_user_mau_growth_180',
                'free_user_util_growth_30',
                'free_user_util_growth_60',
                'free_user_util_growth_90',
                'free_user_util_growth_180',
                'free_meetings_last_30',
                'free_minutes_last_30',
                'free_meetings_per_user_last_30',
                'free_minutes_per_user_last_30',
                'free_minutes_per_meeting_last_30',
                'zr_meetings_last_30',
                'zr_minutes_last_30',
                'zr_meetings_per_zr_last_30',
                'zr_minutes_per_zr_last_30',
                'zr_minutes_per_meeting_last_30',
                'daily_zr_meeting_attach_rate',
                'daily_zr_minutes_attach_rate',
                'webinar_meetings_last_30',
                'webinar_minutes_last_30',
                'webinar_minutes_per_meeting_last_30']
        cat_cols = ['days_left_in_term_range', 'sales_group', 'employee_count', 'coreproduct', 'current_term', 'mrr_entry_range', 'account_age_range']


    df = df.drop(cols, axis=1)
    ###branding the columns

    target = 'churn_gross_next_90'
    IDcol = 'zoom_account_no'
    ahs_date = 'ahs_date'


    ###label encoder for categorical features

    for var in cat_cols:
        number = LabelEncoder()
        df[var] = number.fit_transform(df[var].astype('str'))

    return df


def setup_train_test(df, ratio = 0.35):
    '''
    shuffle and split
    :param df: 
    :return: train, test, both are dataframe
    '''
    from sklearn.utils import shuffle
    df = shuffle(df)
    train, test = train_test_split(df, test_size = ratio)

    return train, test



if __name__ == '__main__':
    train_file = '../data/JanFeb.csv'
    train = pd.read_csv(train_file, sep='\t')

    train = feature_engineer(train)
