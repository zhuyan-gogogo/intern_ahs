# running individual functions in the pipeline
# walk through the pipeline and modify where needed


import pandas as pd
from preprocess import *
from cv_parameter_mod import *
import pickle

from xgboost.sklearn import XGBClassifier
from sklearn.ensemble import GradientBoostingClassifier

###############   TODO: modify this part
# --- ----- ---- ---- setup dataset
separator = ','
group = 'mm'

train_file = '../data/train_data.csv'
test_file = '../data/test_data.csv'

# choose the ml model - xgboost, gbm
ml_model = 'xgboost'
model_file = '../data/train_models_1.model'

# # Coarse tune 1
# param_test = {
#         'max_depth': range(3, 10, 2),
#         'min_child_weight': range(1, 6, 2)}
# {'max_depth': 9, 'min_child_weight': 4}

# Tune 2

# Tune 3
param_test = {'gamma':[i/10.0 for i in range(0,5)],
              'subsample':[i/100.0 for i in range(80, 105,5)],
              'colsample_bytree': [i/100.0 for i in range(65,85,5)]}

run_single_model = True

###############   Running model
print ('----- ----- loading data ')
import warnings
warnings.filterwarnings("ignore",category=DeprecationWarning)

names = ['ahs_date', 'zoom_account_no', 'account_name', 'account_created_date', 'account_age', 'sales_group',
         'employee_count', 'coreproduct', 'currentterm',
         'data_com_industry', 'billtostate', 'billtocountry', 'mrr_entry', 'mrr_exit', 'mrr_growth_30',
         'mrr_growth_60', 'mrr_growth_90', 'mrr_growth_180', 'billing_hosts',
         'paid_user_count', 'billing_hosts_utilization', 'paid_user_mau', 'paid_user_utilization',
         'free_user_count', 'free_user_mau', 'free_user_utilization',
         'total_zr_deployed', 'total_meetings_last_30', 'total_minutes_last_30', 'meetings_per_user_last_30',
         'minutes_per_user_last_30', 'total_minutes_per_meeting_last_30',
         'paid_meetings_last_30', 'paid_minutes_last_30', 'paid_meetings_per_user_last_30',
         'paid_minutes_per_user_last_30', 'paid_minutes_per_meeting_last_30',
         'free_meetings_last_30', 'free_minutes_last_30', 'free_meetings_per_user_last_30',
         'free_minutes_per_user_last_30', 'free_minutes_per_meeting_last_30',
         'zr_meetings_last_30', 'zr_minutes_last_30', 'zr_meetings_per_zr_last_30', 'zr_minutes_per_zr_last_30',
         'zr_minutes_per_meeting_last_30', 'daily_zr_meeting_attach_rate',
         'daily_zr_minutes_attach_rate', 'webinar_meetings_last_30', 'webinar_minutes_last_30',
         'webinar_minutes_per_meeting_last_30', 'unique_host_last_30days',
         'unique_logins_last_30days', 'billing_hosts_growth_30', 'billing_hosts_growth_60',
         'billing_hosts_growth_90', 'billing_hosts_growth_180', 'billing_hosts_util_growth_30',
         'billing_hosts_util_growth_60', 'billing_hosts_util_growth_90', 'billing_hosts_util_growth_180',
         'free_mthly_min_growth_30', 'free_mthly_min_growth_60',
         'free_mthly_min_growth_90', 'free_mthly_min_growth_180', 'free_mthly_min_per_mtg_growth_30',
         'free_mthly_min_per_mtg_growth_60', 'free_mthly_min_per_mtg_growth_90',
         'free_mthly_min_per_mtg_growth_180', 'free_mthly_min_per_user_growth_30',
         'free_mthly_min_per_user_growth_60', 'free_mthly_min_per_user_growth_90',
         'free_mthly_min_per_user_growth_180', 'free_mthly_mtgs_growth_30', 'free_mthly_mtgs_growth_60',
         'free_mthly_mtgs_growth_90', 'free_mthly_mtgs_growth_180',
         'free_mthly_mtgs_per_user_growth_30', 'free_mthly_mtgs_per_user_growth_60',
         'free_mthly_mtgs_per_user_growth_90', 'free_mthly_mtgs_per_user_growth_180',
         'free_user_count_growth_30', 'free_user_count_growth_60', 'free_user_count_growth_90',
         'free_user_count_growth_180', 'free_user_mau_growth_30', 'free_user_mau_growth_60',
         'free_user_mau_growth_90', 'free_user_mau_growth_180', 'free_user_util_growth_30',
         'free_user_util_growth_60', 'free_user_util_growth_90', 'free_user_util_growth_180',
         'mthly_min_per_mtg_growth_30', 'mthly_min_per_mtg_growth_60', 'mthly_min_per_mtg_growth_90',
         'mthly_min_per_mtg_growth_180', 'mthly_min_per_user_growth_30',
         'mthly_min_per_user_growth_60', 'mthly_min_per_user_growth_90', 'mthly_min_per_user_growth_180',
         'mthly_mtgs_per_user_growth_30', 'mthly_mtgs_per_user_growth_60',
         'mthly_mtgs_per_user_growth_90', 'mthly_mtgs_per_user_growth_180', 'paid_mthly_min_growth_30',
         'paid_mthly_min_growth_60', 'paid_mthly_min_growth_90',
         'paid_mthly_min_growth_180', 'paid_mthly_min_per_mtg_growth_30', 'paid_mthly_min_per_mtg_growth_60',
         'paid_mthly_min_per_mtg_growth_90', 'paid_mthly_min_per_mtg_growth_180',
         'paid_mthly_min_per_user_growth_30', 'paid_mthly_min_per_user_growth_60',
         'paid_mthly_min_per_user_growth_90', 'paid_mthly_min_per_user_growth_180',
         'paid_mthly_mtgs_growth_30', 'paid_mthly_mtgs_growth_60', 'paid_mthly_mtgs_growth_90',
         'paid_mthly_mtgs_growth_180', 'paid_mthly_mtgs_per_user_growth_30',
         'paid_mthly_mtgs_per_user_growth_60', 'paid_mthly_mtgs_per_user_growth_90',
         'paid_mthly_mtgs_per_user_growth_180', 'paid_user_count_growth_30', 'paid_user_count_growth_60',
         'paid_user_count_growth_90', 'paid_user_count_growth_180', 'paid_user_mau_growth_30',
         'paid_user_mau_growth_60', 'paid_user_mau_growth_90', 'paid_user_mau_growth_180',
         'paid_user_util_growth_30', 'paid_user_util_growth_60', 'paid_user_util_growth_90',
         'paid_user_util_growth_180', 'total_mthly_min_growth_30', 'total_mthly_min_growth_60',
         'total_mthly_min_growth_90', 'total_mthly_min_growth_180', 'total_mthly_mtgs_growth_30',
         'total_mthly_mtgs_growth_60', 'total_mthly_mtgs_growth_90', 'total_mthly_mtgs_growth_180',
         'opp_amount', 'days_left_in_term', 'churn_downsell', 'churn_cancel', 'churn_gross',
         'churn_downsell_next_90', 'churn_cancel_next_90', 'churn_gross_next_90']

train = pd.read_csv(train_file, sep = separator, names=names)
test = pd.read_csv(train_file, sep = separator, names=names)


all_train = feature_engineer(train, group = group)
train, validate = setup_train_test(all_train)


target = 'churn_gross_next_90'
IDcol = 'zoom_account_no'
predictors = [x for x in train.columns if x not in [target, IDcol]]


test = feature_engineer(test, group = group)


# --- ----- ---- ---- run model with default, the report will be printed

if ml_model == 'gbm':
    print('---- ----- running built in Gradient Boosting model --- ---- ')
    alg = GradientBoostingClassifier(learning_rate=0.1,
                                     n_estimators=50,
                                     min_samples_split=200,
                                     subsample=0.85,
                                     max_features=45)

elif ml_model == 'xgboost':
    print('---- ----- running built in XGBoosting model --- ---- ')
    alg = XGBClassifier(
        learning_rate=0.1,
        n_estimators=100,
        max_depth=10,
        min_child_weight=4,
        gamma=0.2,
        subsample=0.85,
        colsample_bytree=0.75,
        objective='binary:logistic',
        scale_pos_weight=1)

if run_single_model:
    pred_train = eval_auc(alg, train, test)

print('---- ----- tunning parameter... ')
print param_test
# --- ----- ---- ---- tune the parameter, the report will be printed
cv_models = tune_general(alg, train, param= param_test)

print ('#--- ---- unseen data')
pred_unseen = eval_auc(cv_models, train, validate)

print ('#--- ---- unseen future data')
pred_unseen = eval_auc(cv_models, train, test)

pickle.dump(cv_models, open(model_file, 'wb'))



