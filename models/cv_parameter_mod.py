#################################################
############# building the model ################
#################################################
'''
this script cross validate models for parameter tunning 

calling python script
python cv_parameter.py # will run cv on toy dataset in ../data/
python cv_parameter.py training_file.csv save_model_to_file.model # will save the RandomizedSearchCV model to target file

functions 
cv_precision_recall(train) # train is a dataframe, macro precision and recall, recall shows better performance
cv_f1(train) # used another scoring to cv 
cv_xgboost_auc(train) #

tunning workflow: 
1. 
- set relatively high learning rate. 0.05-0.3
- optimum number of trees for this learning rate, XGBoost cv
2.
- tune tree-specific parameters, max_depth, min_child_weight, gamma, subsample, colsample_bytree
3.
- tune regularization parameters (lambda, alpha)
4. 
- Lower the learning rate

first set of params: 
max_depth = 5 : between 3-10. 
min_child_weight = 1 : 
gamma = 0 : A smaller value like 0.1-0.2. will be tuned later.
subsample, colsample_bytree = 0.8 : commonly used used start value. typical: 0.5-0.9.
scale_pos_weight = 1: high class imbalance.


'''


import io
import time
import sys
import pickle
import pandas as pd
import numpy as np
#import matplotlib.pylab as plt

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from sklearn.metrics import classification_report

from xgboost.sklearn import XGBClassifier
from preprocess import *

import warnings

###branding the columns

def tunning_xgboost(train, test):
    # build the default xgboost model
    alg = build_xgboost(train, test, False)
    # tune parameters
    cvModels = tune_general(alg, train)

    return cvModels

def build_xgboost(train, test, performCV=True):
    '''
    
    :param train: 
    :return: 
    '''
    xgb1 = XGBClassifier(
        learning_rate=0.1,
        n_estimators=100,
        max_depth=10,
        min_child_weight=4,
        gamma=0.2,
        subsample=0.85,
        colsample_bytree=0.75,
        objective='binary:logistic',
        scale_pos_weight=1,
        random_state=27)

    # Fit the algorithm on the data
    # evaluate with cross validation, print the report
    print('---- ----- running built in XGboosting model --- ---- ')
    predicted = eval_auc(xgb1, train, test, performCV)

    return xgb1


def eval_auc(alg, train, test, performCV=True, cv_folds=10):
    '''

    :param alg:
    :param train:
    :param predictors:
    :param target:
    :param performCV:
    :param cv_folds:
    :return: predicted labels
    '''

    target = 'churn_gross_next_90'
    IDcol = 'zoom_account_no'
    predictors = [x for x in test.columns if x not in [target, IDcol]]

    # Predict training set:
    test_pred = alg.fit(train[predictors], train[target]).predict(test[predictors])
    # test_predprob = alg.predict_proba(test[predictors])[:,? 1]

    # Perform cross-validation:
    if performCV:
        cv_score = cross_val_score(alg, test[predictors], test[target], cv=cv_folds,scoring='roc_auc')

    # Print model report:
    print "\nModel Report"
    print "Accuracy : %.4g" % metrics.accuracy_score(test[target].values, test_pred)
    print "AUC Score (Train): %f" % metrics.roc_auc_score(test[target], test_pred)
    print (classification_report(test[target], test_pred))
    print pd.crosstab(test['churn_gross_next_90'], test_pred, rownames=['True'], colnames=['Predicted'], margins=True)

    # print pd.crosstab(validate['churn_gross_next_gross_next_90'], val_pred, rownames=['True'], colnames=['Predicted'], margins=True)


    if performCV:
        print "CV Score : Mean - %.7g | Std - %.7g | Min - %.7g | Max - %.7g" % (
        np.mean(cv_score), np.std(cv_score), np.min(cv_score), np.max(cv_score))

    return test_pred

    # # Print Feature Importance:
    # if printFeatureImportance:
    #     feat_imp = pd.Series(alg.feature_importances_, predictors).sort_values(ascending=False)
    #     feat_imp.plot(kind='bar', title='Feature Importances')
    #     plt.ylabel('Feature Importance Score')

def tune_precision_recall(alg, train):
    print ('training set: ')
    print (train.shape)

    target = 'churn_gross_next_90'
    IDcol = 'zoom_account_no'
    predictors = [x for x in train.columns if x not in [target, IDcol]]

    sampleSize = train.shape[0]
    fetureSize = train.shape[1]
    # param_test1 = {'min_samples_split': range(int(0.005*sampleSize), int(0.02*sampleSize)),
    #                'max_depth': range(5, 50), 'min_samples_leaf': range(5, 31),
    #                'n_estimator': range(100, 700, 50)}

    param_test1 = {'min_samples_split': range(int(0.005*sampleSize), int(0.01*sampleSize)),
                   'max_depth': range(25, 50), 'min_samples_leaf': range(25, 31),
                   'n_estimators': range(400, 700, 50)}

    scores = ['precision', 'recall']

    for score in scores:
        print("# Tuning hyper-parameters for %s" % score)
        # macro recall shows better performan
        gsearch1 = RandomizedSearchCV(estimator=alg, param_distributions=param_test1, scoring='%s_macro' % score, cv=10)

        t0 = time.time()
        gsearch1.fit(train[predictors], train[target])
        dt = time.time() - t0
        print (time.strftime("%H:%M:%S", time.gmtime(int(dt))), ' for cross validation')

        bestParams = gsearch1.best_params_
        print (bestParams)

        # a single run for best parameter
        y_true = train[target]
        y_pred = gsearch1.predict(train[predictors])
        print classification_report(y_pred, y_true)

        # cv results
        means = gsearch1.cv_results_['mean_test_score']
        stds = gsearch1.cv_results_['std_test_score']

        for mean, std, params in zip(means, stds, gsearch1.cv_results_['params']):
            print("%0.3f (+/-%0.03f) for %r"
                  % (mean, std * 2, params))

        gsearch1.best_score_

        return gsearch1


def tune_general(alg, train, scoring='roc_auc',
                 param = {'gamma':[i/10.0 for i in range(0,5)], 'subsample':[i/100.0 for i in range(80, 105,5)]}):
    sampleSize = train.shape[0]
    fetureSize = train.shape[1]

    target = 'churn_gross_next_90'
    IDcol = 'zoom_account_no'
    predictors = [x for x in train.columns if x not in [target, IDcol]]

    # ---- ---- ----- ----- tuning for gradient boosting

    # param_test1 = {'min_samples_split': range(int(0.005*sampleSize), int(0.02*sampleSize)),
    #                'max_depth': range(5, 50), 'min_samples_leaf': range(5, 31),
    #                'n_estimator': range(100, 700, 50)}

    # param_test1 = {'min_samples_split': range(int(0.005*sampleSize), int(0.01*sampleSize)),
    #                'max_depth': range(25, 50), 'min_samples_leaf': range(25, 31),
    #                'n_estimators': range(400, 700, 50)}

    # ---- ---- ----- ----- tuning for xgboost
    param_test1 = param

    #param_test1 = {'max_depth': range(8, 11), 'min_child_weight': range(3, 6), 'gamma':[i/10.0 for i in range(0,3)], 'subsample':[i/100.0 for i in range(75,90,5)], 'colsample_bytree':[i/100.0 for i in range(75,90,5)]}

     # param_test1 = {'max_depth': range(3, 10), 'min_child_weight': range(1, 6)}
    print "# ---- ----- ----- Tuning hyper-parameters %s scoring" %scoring, param_test1.keys()

    gsearch1 = RandomizedSearchCV(estimator=alg, param_distributions=param_test1, scoring= scoring, cv=10, verbose=2)

    t0 = time.clock()
    gsearch1.fit(train[predictors], train[target])
    dt = time.time() - t0
    print (time.strftime("%H:%M:%S", time.gmtime(dt)), ' for cross validation')

    # cv results
    means = gsearch1.cv_results_['mean_test_score']
    stds = gsearch1.cv_results_['std_test_score']

    for mean, std, params in zip(means, stds, gsearch1.cv_results_['params']):
        print("%0.3f (+/-%0.03f) for %r"
              % (mean, std * 2, params))

    return gsearch1


# NOT USED. customrized scoring function
def score_func(y_true, y_pred, **kwarge):
    labels = np.unique(y_true)
    tp = sum(y_true == labels[1] and y_pred == labels[1])
    tn = sum(y_true == labels[0] and y_pred == labels[0])
    # true positive



# todo: implement xgboost (has regularization to prevent overfitting, parallel processing)
# todo: use hyper parameter tunning, bayes tunning



if __name__ == '__main__':
    # --- ---- ---- ----- ---- ---- ----- setup data
    '''
    cv_parameter.py input_file.csv model_file.model group
    '''

    names = ['ahs_date', 'zoom_account_no', 'account_name', 'account_created_date', 'account_age', 'sales_group',
             'employee_count', 'coreproduct', 'currentterm',
             'data_com_industry', 'billtostate', 'billtocountry', 'mrr_entry', 'mrr_exit', 'mrr_growth_30',
             'mrr_growth_60', 'mrr_growth_90', 'mrr_growth_180', 'billing_hosts',
             'paid_user_count', 'billing_hosts_utilization', 'paid_user_mau', 'paid_user_utilization',
             'free_user_count', 'free_user_mau', 'free_user_utilization',
             'total_zr_deployed', 'total_meetings_last_30', 'total_minutes_last_30', 'meetings_per_user_last_30',
             'minutes_per_user_last_30', 'total_minutes_per_meeting_last_30',
             'paid_meetings_last_30', 'paid_minutes_last_30', 'paid_meetings_per_user_last_30',
             'paid_minutes_per_user_last_30', 'paid_minutes_per_meeting_last_30',
             'free_meetings_last_30', 'free_minutes_last_30', 'free_meetings_per_user_last_30',
             'free_minutes_per_user_last_30', 'free_minutes_per_meeting_last_30',
             'zr_meetings_last_30', 'zr_minutes_last_30', 'zr_meetings_per_zr_last_30', 'zr_minutes_per_zr_last_30',
             'zr_minutes_per_meeting_last_30', 'daily_zr_meeting_attach_rate',
             'daily_zr_minutes_attach_rate', 'webinar_meetings_last_30', 'webinar_minutes_last_30',
             'webinar_minutes_per_meeting_last_30', 'unique_host_last_30days',
             'unique_logins_last_30days', 'billing_hosts_growth_30', 'billing_hosts_growth_60',
             'billing_hosts_growth_90', 'billing_hosts_growth_180', 'billing_hosts_util_growth_30',
             'billing_hosts_util_growth_60', 'billing_hosts_util_growth_90', 'billing_hosts_util_growth_180',
             'free_mthly_min_growth_30', 'free_mthly_min_growth_60',
             'free_mthly_min_growth_90', 'free_mthly_min_growth_180', 'free_mthly_min_per_mtg_growth_30',
             'free_mthly_min_per_mtg_growth_60', 'free_mthly_min_per_mtg_growth_90',
             'free_mthly_min_per_mtg_growth_180', 'free_mthly_min_per_user_growth_30',
             'free_mthly_min_per_user_growth_60', 'free_mthly_min_per_user_growth_90',
             'free_mthly_min_per_user_growth_180', 'free_mthly_mtgs_growth_30', 'free_mthly_mtgs_growth_60',
             'free_mthly_mtgs_growth_90', 'free_mthly_mtgs_growth_180',
             'free_mthly_mtgs_per_user_growth_30', 'free_mthly_mtgs_per_user_growth_60',
             'free_mthly_mtgs_per_user_growth_90', 'free_mthly_mtgs_per_user_growth_180',
             'free_user_count_growth_30', 'free_user_count_growth_60', 'free_user_count_growth_90',
             'free_user_count_growth_180', 'free_user_mau_growth_30', 'free_user_mau_growth_60',
             'free_user_mau_growth_90', 'free_user_mau_growth_180', 'free_user_util_growth_30',
             'free_user_util_growth_60', 'free_user_util_growth_90', 'free_user_util_growth_180',
             'mthly_min_per_mtg_growth_30', 'mthly_min_per_mtg_growth_60', 'mthly_min_per_mtg_growth_90',
             'mthly_min_per_mtg_growth_180', 'mthly_min_per_user_growth_30',
             'mthly_min_per_user_growth_60', 'mthly_min_per_user_growth_90', 'mthly_min_per_user_growth_180',
             'mthly_mtgs_per_user_growth_30', 'mthly_mtgs_per_user_growth_60',
             'mthly_mtgs_per_user_growth_90', 'mthly_mtgs_per_user_growth_180', 'paid_mthly_min_growth_30',
             'paid_mthly_min_growth_60', 'paid_mthly_min_growth_90',
             'paid_mthly_min_growth_180', 'paid_mthly_min_per_mtg_growth_30', 'paid_mthly_min_per_mtg_growth_60',
             'paid_mthly_min_per_mtg_growth_90', 'paid_mthly_min_per_mtg_growth_180',
             'paid_mthly_min_per_user_growth_30', 'paid_mthly_min_per_user_growth_60',
             'paid_mthly_min_per_user_growth_90', 'paid_mthly_min_per_user_growth_180',
             'paid_mthly_mtgs_growth_30', 'paid_mthly_mtgs_growth_60', 'paid_mthly_mtgs_growth_90',
             'paid_mthly_mtgs_growth_180', 'paid_mthly_mtgs_per_user_growth_30',
             'paid_mthly_mtgs_per_user_growth_60', 'paid_mthly_mtgs_per_user_growth_90',
             'paid_mthly_mtgs_per_user_growth_180', 'paid_user_count_growth_30', 'paid_user_count_growth_60',
             'paid_user_count_growth_90', 'paid_user_count_growth_180', 'paid_user_mau_growth_30',
             'paid_user_mau_growth_60', 'paid_user_mau_growth_90', 'paid_user_mau_growth_180',
             'paid_user_util_growth_30', 'paid_user_util_growth_60', 'paid_user_util_growth_90',
             'paid_user_util_growth_180', 'total_mthly_min_growth_30', 'total_mthly_min_growth_60',
             'total_mthly_min_growth_90', 'total_mthly_min_growth_180', 'total_mthly_mtgs_growth_30',
             'total_mthly_mtgs_growth_60', 'total_mthly_mtgs_growth_90', 'total_mthly_mtgs_growth_180',
             'opp_amount', 'days_left_in_term', 'churn_downsell', 'churn_cancel', 'churn_gross',
             'churn_downsell_next_90', 'churn_cancel_next_90', 'churn_gross_next_90']

    print (len(sys.argv))
    # --- ---- ---- ----- ---- ---- ----- setup data
    if len(sys.argv) < 2:
        print ('running testing cv')
        train_file = '../data/JanFeb.csv'
        test_file = '../data.March.csv'
        model_file = '../models/JanFeb_cv.model'
        group = 'sij'
        sep = '\t'

    elif len(sys.argv) == 4:
        # train_file='../data/train_data.csv'
        # model_file='../data/train_models.model'
        group='mm'
        train_file = sys.argv[1]
        test_file = '../data/test_data.csv' 
        model_file = sys.argv[2]
        group = sys.argv[3]
        sep = ','

    else:
        s3 = boto3.client('s3')
        data_file = sys.argv[1]
        obj = s3.get_object(Bucket='zoom-datascience', Key='acct_health_score/' + 'tabledata/' + data_file)
        sep = ','

    df = pd.read_csv(train_file, sep=sep, names= names)
    df = feature_engineer(df, group=group)

    df_new = pd.read_csv(test_file, sep=sep, names= names)
    df_new = feature_engineer(df_new, group=group)
    train, test = setup_train_test(df)
    
    target = 'churn_gross_next_90'
    IDcol = 'zoom_account_no'
    predictors = [x for x in train.columns if x not in [target, IDcol]]
    print ('# ---- ----- results on future data')
    xgboost_last = build_xgboost(df, df_new)
    #eval_auc(xgboost_last, df, df_new, True)

    print 'training set: ', train.shape
    print 'testing set: ', test.shape
    print 'number of predictors: ', len(predictors)

    print ('#---- ---- results on unseen test data')
    cv_models = tunning_xgboost(train, test)

    bestParams = cv_models.best_params_

    print (bestParams)

    eval_auc(cv_models, train, test, False)

    # # single run for best parameters
    # y_true = test[target]
    # y_pred = cv_models.predict(test[predictors])
    # print classification_report(y_pred, y_true)
    # print ("AUC Score (Additional Test): %f" %metrics.roc_auc_score(y_true, y_pred))

    pickle.dump(cv_models, open(model_file,'wb'))







