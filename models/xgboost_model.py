###64GB ec2 instance
#ssh ec2-user@52.91.193.93

###32GB ec2 instance
#ssh ec2-user@54.174.221.32

###run python in datascience instance
#sudo su - datascience
#python

# import boto3

import io

import pickle
import pandas as pd
import numpy as np
import time

import xgboost as xgb

# evaluation
from sklearn.metrics import classification_report, precision_recall_fscore_support
from sklearn.metrics import roc_auc_score, accuracy_score, auc
from copy import deepcopy
'''
gradient boosting 
advantage:
- parallel computing 
- regularization to reduce overfitting 

parameters:


'''
from preprocess import *

# s3 = boto3.client('s3')
# # Replace the path of the file name as per requirement
# obj = s3.get_object(Bucket='zoom-datascience',Key = 'acct_health_score/' + 'tabledata/' + 'febmarch.csv')
# train_file = obj["Body"].read()


# ---------- ---------- ---------- ---------- ---------- load data
train_file = '../data/JanFeb.csv'
test_file = '../data/March.csv'

train = pd.read_csv(train_file, sep='\t')
test = pd.read_csv(test_file, sep='\t')


train = feature_engineer(train)
test = feature_engineer(test)


# ---------- ---------- ---------- ---------- ---------- load data
# train, test = train_test_split(df, test_size = 0.35)
pd.value_counts(train['churn_gross_next_90'].values, sort=False)
pd.value_counts(test['churn_gross_next_90'].values, sort=False)

target = 'churn_gross_next_90'
ahs_date = 'ahs_date'
IDcol = 'zoom_account_no'

predictors = [x for x in train.columns if x not in [target, IDcol, ahs_date]]


gbm = xgb.GradientBoostingClassifier(max_depth=3,
                        n_estimators=300,
                        learning_rate=0.05).fit(train[predictors], train[target])

t0  = time.time()
y_true=test[target]
y_pred = gbm.predict(test[predictors])
print (time.time() - t0)


'''

{'max_depth': 41,
 'min_samples_leaf': 26,
 'min_samples_split': 11,
 'n_estimators': 550}

'''

print "\nModel Report on Test Dataset"
print "Accuracy : %.4g" % accuracy_score(test['churn_gross_next_90'], y_pred)
print "AUC Score (Test): %f" % roc_auc_score(test['churn_gross_next_90'], y_pred)

print pd.crosstab(test['churn_gross_next_90'], y_pred, rownames=['True'], colnames=['Predicted'], margins=True)
print (classification_report(y_true, y_pred))

precision, recall, fscore, support  = precision_recall_fscore_support(y_true, y_pred)
