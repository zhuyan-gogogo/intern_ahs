###64GB ec2 instance
#ssh ec2-user@52.91.193.93

###32GB ec2 instance
#ssh ec2-user@54.174.221.32

###run python in datascience instance
#sudo su - datascience
#python

import io
import boto3
import pickle
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import cross_validation, metrics
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score

# evaluation
from sklearn.metrics import classification_report, precision_recall_fscore_support

from copy import deepcopy


from preprocess import *

# s3 = boto3.client('s3')
# # Replace the path of the file name as per requirement
# obj = s3.get_object(Bucket='zoom-datascience',Key = 'acct_health_score/' + 'tabledata/' + 'febmarch.csv')
# train_file = obj["Body"].read()


# ---------- ---------- ---------- ---------- ---------- load data
train_file = '../data/JanFeb.csv'
test_file = '../data/March.csv'

train = pd.read_csv(train_file, sep='\t')
test = pd.read_csv(test_file, sep='\t')


train = feature_engineer(train)
test = feature_engineer(test)


# train, test = train_test_split(df, test_size = 0.35)
pd.value_counts(train['churn_gross_next_90'].values, sort=False)
pd.value_counts(test['churn_gross_next_90'].values, sort=False)

target = 'churn_gross_next_90'
ahs_date = 'ahs_date'
IDcol = 'zoom_account_no'

predictors = [x for x in train.columns if x not in [target, IDcol, ahs_date]]

gbm_tuned = GradientBoostingClassifier(learning_rate=0.075,
                                         n_estimators=700,
                                         max_depth=45,
                                         min_samples_split=150,
                                         min_samples_leaf=20,
                                         subsample=0.65,
                                         random_state=10,
                                         max_features=45)

ahs_3_seg_model= gbm_tuned.fit(train[predictors], train['churn_gross_next_90'])

'''

{'max_depth': 41,
 'min_samples_leaf': 26,
 'min_samples_split': 11,
 'n_estimators': 550}

'''

# checking the accuracy of the model
y_pred = ahs_3_seg_model.predict(test[predictors])
y_true = test['churn_gross_next_90']

print "\nModel Report on Test Dataset"
print "Accuracy : %.4g" % metrics.accuracy_score(test['churn_gross_next_90'], y_pred)
print "AUC Score (Test): %f" % metrics.roc_auc_score(test['churn_gross_next_90'], y_pred)

print pd.crosstab(test['churn_gross_next_90'], y_pred, rownames=['True'], colnames=['Predicted'], margins=True)
print (classification_report(y_true, y_pred))

precision, recall, fscore, support  = precision_recall_fscore_support(y_true, y_pred)
# ------ ------- ------ ------- ------ ------- ------ alternative evaluation method
# todo: precision-recall curve
# todo: draw ROC-AUC graph
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize

