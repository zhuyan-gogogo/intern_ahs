from __future__ import print_function
from __future__ import division
from datetime import datetime, date, timedelta
import sys
import psycopg2

import ProcessConstantsDS

import boto3
import pandas as pd

conn = psycopg2.connect(
    host=ProcessConstantsDS.host,
    user=ProcessConstantsDS.user,
    port=ProcessConstantsDS.port,
    password=ProcessConstantsDS.password,
    dbname=ProcessConstantsDS.dbname)
print("Database Connection Successfull")
cur = conn.cursor()


SB = 'SB'
Int = 'Int'
Just_Me = 'Just Me'
#now = datetime.datetime.now()
#dt =  now.strftime("%Y-%m-%d")
now= datetime.today() - timedelta(days=2)
dt = now.strftime("%Y-%m-%d")



q = """select * from etl_acct_health.ahs_3_ml_ad where sales_group in ('%s','%s','%s') and ahs_date = '%s'""" % (SB,Int,Just_Me,dt)
df_q = pd.read_sql(q, conn)
df_q.to_csv('/home/datascience/ml_model/inputData/test_data.csv', index=False)


# get the toy dataset
# select * from etl_acct_health.ahs_3_ml_ad where sales_group = 'SB' and ahs_date >= '2017-01-01' and ahs_date <= '2017-03-01' limit 2000

